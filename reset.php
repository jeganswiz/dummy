<?php 
include 'header.php';
?>
<div class="container">
    <div class="row">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center">
                          <h3><i class="fa fa-smile-o fa-4x"></i></h3>
                          <h2 class="text-center">Reset Password!</h2>
                          <p>You can reset your password here.</p>
                            <div class="panel-body">
                              
                              <form class="form">
                                <fieldset>
                                  <div class="form-group">
                                    
                                      
                                      <input id="verify-phone" placeholder="Phone Number" class="form-control" type="hidden">
                                    
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="glyphicon glyphicon-lock color-blue"></i></span>
                                       
                                      <input id="password-field" placeholder="Enter your Newpassword" class="form-control" width="20px" name="password" type="password"><span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                    </div>
                                    <br>
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="glyphicon glyphicon-check color-blue"></i></span>
                                      
                                      <input id="verify-otp" placeholder="Enter OTP" class="form-control" type="text">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <input class="btn btn-lg btn-primary btn-block" id="reset-btn" value="Set Password.." type="button">
                                    <p id="reset-err">-</p>
                                  </div>
                                </fieldset>
                              </form>
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
include 'footer.php';
?>