<?php 
include 'header.php';
?>
<div class="container">
    <div class="row">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center">
                          <h3><i class="fa fa-frown-o fa-4x"></i></h3>
                          <h2 class="text-center">Forgot Password?</h2>
                          <p>Fill the following Field.</p>
                            <div class="panel-body">
                              
                              <form class="form" method="post" action="#">
                                <fieldset>
                                  <div class="form-group">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="glyphicon glyphicon-earphone color-blue"></i></span>
                                      
                                      <input id="verify-phone" placeholder="Phone Number" class="form-control" type="text">
                                  
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <input class="btn btn-lg btn-primary btn-block" id="forgot-btn" value="Verify Number" type="button">
                                    <p id="forgot-err">-</p>
                                  </div>
                                </fieldset>
                              </form>
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
include 'footer.php';
?>