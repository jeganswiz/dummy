var city = [ 
        {        
        id: 1,
        title: 'Andhra Pradesh',
        subs: [
            {
                id: 10,
                title: 'Anantapur'
            }, {
                id: 11,
                title: 'Chittoor'
            }, {
                id: 12,
                title: 'East Godavari'
            },{
                id:13,
                title: 'Guntur'
            },{
                id: 14,
                title: 'Krishna'
            }, {
                id: 15,
                title: 'Kurnool'
            }, {
                id: 16,
                title: 'Prakasam'
            }, {
                id: 17,
                title: 'Srikakulam'
            }, {
                id: 18,
                title: 'Sri Potti Sriramulu Nellre'
            }, {
                id: 19,
                title: 'Visakhapatnam'
            }, {
                id: 20,
                title: 'Vizianagaram'
            }, {
                id: 21,
                title: 'West Godavari'
            }, {
                id: 22,
                title: 'Y.S.R.(Cuddaah)'
            }, 
        ]
    },
    {
        id: 2,
        title: 'Arunachal Pradesh',
        subs: [
            {
                id: 20,
                title: 'Along'
            }, {
                id: 21,
                title: 'Basar'
            }, {
                id: 22,
                title: 'Bomdila'
            }, {
                id: 23,
                title: 'Changlang'
            }, {
                id: 24,
                title: 'Daporijo'
            }, {
                id: 25,
                title: 'Deomali'
            }, {
                id: 26,
                title: 'Itanagar'
            }, {
                id: 27,
                title: 'Jairampur'
            },  {
                id: 28,
                title: 'Khonsa'
            }, {
                id: 29,  
                title: 'Naharlagun'
            }, {
                id: 201,
                title: 'Namsai'
            }, {
                id: 202,
                title: 'Pasighat'
            }, {
                id: 203,
                title: 'Roing'
            }, {
                id: 204,
                title: 'Seppa'
            }, {
                id: 205,
                title: 'Tawang'
            }, {
                id: 206,
                title:'Tezu'
            }, {
                id: 207,
                title: 'Ziro'
            },

        ]

    },
    {
        id:3,
        title: 'Assam',
         subs: [
            {
                id:1,
                title: 'Udalguri'
            }, {
                id:2,
                title: 'karimganj'
            }, {
                id:3,
                title: 'cachar'
            }, {
                id:4,
                title:'kamrup'
 
            },
             {
                id:5,
                title:'kamrup Metro'
 
            },
             {
                id:6,
                title:'Karbi Anglong'
 
            },
             {
                id:7,
                title:'Kokrajhar'
 
            },
             {
                id:8,
                title:'golaghat'
 
            },
             {
                id:9,
                title:'Goalpara'
 
            },
             {
                id:10,
                title:'Chirang'
 
            },
             {
                id:11,
                title:'Dibrugarh'
 
            },
             {
                id:12,
                title:'DimaHasao'
 
            },
             {
                id:13,
                title:'Tinsukia'
 
            },
            {
                id:14,
                title:'Darrang'
 
            },
             {
                id:15,
                title:'Dhubri'
 
            },
            {
                id:16,
                title:'Dhemaji'
 
            },
             {
                id:17,
                title:'Nagaon'
 
            },
            {
                id:18,
                title:'Nalbari'
 
            },
            {
                id:19,
                title:'Bongaigaon'
 
            },
            {
                id:20,
                title:'Barpeta'
 
            },
            {
                id:21,
                title:'Baksa'
 
            },
            {
                id:22,
                title:'Morigaon'
 
            },
            {
                id:23,
                title:'Jorhat'
 
            },
            {
                id:24,
                title:'Lakhimpur'
 
            },
            {
                id:25,
                title:'Sivasagar'
 
            },
            {
                id:26,
                title:'Sonitpur'
 
            },
            {
                id:27,
                title:'Hailakandi'
 
            },
     ]

    },
     {
        id: 4,
        title: 'Bihar',
         subs: [
            {
                id: 1,
                title: 'Araria'
            }, {
                id: 2,
                title: 'Arwal'
            }, {
                id: 3,
                title: 'Aurangaband'
            }, {
                id:4,
                title: 'Banka'
            }, {
                id:5,
                title: 'Begusarai'
            }, {
                id:6,
                title: 'Bhagalpur'
            }, {
                id:7,
                title: 'Bhojpur'
            }, {
                id:8,
                title: 'Buxar'
            },  {
                id:9,
                title: 'Darbhanga'
            }, {
                id:10,  
                title: 'Gaya'
            }, {
                id:11,
                title: 'Gopalganj'
            }, {
                id:12,
                title: 'Jamui'
            }, {
                id:13,
                title: 'Jehanabad'
            }, {
                id:14,
                title: 'Kaimur'
            }, {
                id:15,
                title: 'Katihur'
            }, {
                id:16,
                title:'Khagaria'
            }, {
                id:17,
                title: 'Kishanganj'
            }, {
                id:18,
                title:'Lakhisarai'
            }, {
                id:19,
                title: 'Madhepura'
            }, {
                id:20,
                title: 'Madhubani'
            }, {
                id:21,
                title: 'Munger'
            }, {
                id:22,
                title: 'Muzaffarpur'
            }, {
                id:23,
                title:'Nalanda'
            }, {
                id:24,
                title: 'Nawada'
            }, {
                id:25,
                title: 'Pashchim Champaran'
            }, {
                id:26,
                title: 'Patna'
            }, {
                id:27,
                title: 'Purba Champaran'
            }, {
                id:28,
                title: 'Purnia'
            }, {
                id:29,
                title: 'Rohtas'
            }, {
                id:30,
                title: 'Saharsa'
            }, {
                id:31,
                title: 'Samastipur'
            }, {
                id:32,
                title: 'Saran'
            }, {
                id:33,
                title: 'Sheikhpura'
            }, {
                id:34,
                title: 'Sheohar'
            }, {
                id:35,
                title: 'Sitamarhi'
            }, {
                id:36,
                title: 'Siwan'
            }, {
                id:37,
                title: 'Supaul'
            }, {
                id:38,
                title: 'Vaishali'
            },
        ]
    }, {
        id: 5,
        title: 'Chhattisgarh',
        subs: [
            {
                id: 1,
                title: 'Bastar'
            }, {
                id: 2,
                title: 'Bijapur'
            }, {
                id: 3,
                title: 'Bilaspur'
            }, {
                id: 4,
                title: 'Dakshin Bastar Dantewada'
            }, {
                id: 5,
                title: 'Dhamtari'
            }, {
                id: 6,
                title: 'Janjgir-Champa'
            }, {
                id: 7,
                title: 'Jashpur'
            }, {
                id: 8,
                title: 'Kabirdham'
            },  {
                id: 9,
                title: 'Korba'
            }, {
                id: 10,  
                title: 'Koriya'
            }, {
                id: 11,
                title: 'Mahasamund'
            }, {
                id: 12,
                title: 'Naranyanpur'
            }, {
                id: 13,
                title: 'Raigarh'
            }, {
                id: 14,
                title: 'Raipur'
            }, {
                id: 15,
                title: 'Rajnandgaon'
            }, {
                id: 16,
                title:'Surguja'
            }, {
                id: 17,
                title: 'Uttar Bastar Kanker'
            },

        ]

    }, {
        id: 6,
        title: 'Goa',
        subs: [
            {
                id: 1,
                title: 'Aldona'
            }, {
                id: 2,
                title: 'Anjuna'
            }, {
                id: 3,
                title: 'Aquem'
            }, {
                id: 4,
                title: 'Arambol'
            }, {
                id: 5,
                title: 'Bambolim'
            }, {
                id: 6,
                title: 'Bandora'
            }, {
                id: 7,
                title: 'Benaulim'
            }, {
                id: 8,
                title: 'Bicholim'
            },  {
                id: 9,
                title: 'Borim'
            }, {
                id: 10,  
                title: 'Calangute'
            }, {
                id: 11,
                title: 'Calapur'
            }, {
                id: 12,
                title: 'Canacona'
            }, {
                id: 13,
                title: 'Candola'
            }, {
                id: 14,
                title: 'Candolim'
            }, {
                id: 15,
                title: 'Carapur'
            }, {
                id: 16,
                title:'Chicalim'
            }, {
                id: 17,
                title: 'Chimbel'
            },{
                id: 18,
                title: 'Chinchinim'
            }, {
                id: 19,
                title: 'Colvale'
            },  {
                id: 20,
                title: 'Corlim'
            }, {
                id: 21,
                title: 'Cortalim'
            }, {
                id: 22,
                title: 'Cumbarjua'
            },  {
                id: 23,
                title: 'Cuncolim'
            }, {
                id: 24,
                title: 'Curchorem-Cacora'
            }, {
                id: 25,
                title: 'Curti'
            }, {
                id: 26,
                title: 'Curtorim'
            }, {
                id: 27,
                title: 'Davorlim'
            }, {
                id: 28,
                title: 'Goa Vehla'
            }, {
                id: 29,
                title: 'Guirim'
            }, {
                id: 30,
                title: 'Jua'
            }, {
                id: 31,
                title: 'Mandrem'
            }, {
                id: 32,
                title: 'Mapusa'
            }, {
                id: 33,
                title: 'Marcaim'
            }, {
                id: 34,
                title: 'Margao'
            }, {
                id: 35,
                title: 'Mercurim'
            }, {
                id: 36,
                title: 'Moira'
            }, {
                id: 37,
                title: 'Morjim'
            }, {
                id: 38,
                title: 'Mormugao'
            }, {
                id: 39,
                title: 'Murda'
            }, {
                id: 40,
                title: 'Navelim'
            }, {
                id: 41,
                title: 'Nerul'
            }, {
                id: 42,
                title: 'Nuvem'
            }, {
                id: 43,
                title: 'Onda'
            }, {
                id: 44,
                title: 'Orgao'
            }, {
                id: 45,
                title: 'Pale'
            }, {
                id: 46,
                title: 'Panaji'
            }, {
                id: 47,
                title: 'Paracem'
            }, {
                id: 48,
                title: 'Penha de franca'
            }, {
                id: 49,
                title: 'Pernem'
            }, {
                id: 50,
                title: 'Pilerne'
            }, {
                id: 51,
                title: 'Ponda'
            }, {
                id: 52,
                title: 'Priol'
            }, {
                id: 53,
                title: 'Quela'
            }, {
                id: 54,
                title: 'Quepem'
            }, {
                id: 55,
                title: 'Raia'
            }, {
                id: 56,
                title: 'Reis Magos'
            }, {
                id: 57,
                title: 'Saligao'
            }, {
                id: 58,
                title: 'Salvador do Mundo'
            }, {
                id: 59,
                title: 'Sancoale'
            }, {
                id: 60,
                title: 'Sanguem'
            }, {
                id: 61,
                title: 'Sanquelim'
            }, {
                id: 62,
                title: 'Sanvordem'
            },  {
                id: 63,
                title: 'Sao Jose de Areal'
            }, {
                id: 64,
                title: 'Siolim'
            }, {
                id: 65,
                title: 'Socorro'
            }, {
                id: 66,
                title: 'Usgao'
            }, {
                id: 67,
                title: 'Valpoi'
            }, {
                id: 68,
                title: 'Varca'
            }, {
                id: 69,
                title: 'Verna'
            }, {
                id: 70,
                title: 'Xeldem'
            }, 
        

        ]

    },
    {
        id: 7,
        title: 'Gujarat',
        subs: [
            {
                id: 1,
                title: 'Ahmadabad'
            },{
                id: 2,
                title: 'Amreli'
            }, {
                id: 3,
                title: 'Anand'
            }, {
                id: 4,
                title: 'Banas Kantha'
            }, {
                id: 5,
                title: 'Bharuch'
            }, {
                id: 6,
                title: 'Bhavnagar'
            }, {
                id: 7,
                title: 'Dohad'
            }, {
                id: 8,
                title: 'Gandhinagar'
            }, {
                id: 9,
                title: 'Jamnagar'
            }, {
                id: 10,
                title: 'Junagadh'
            }, {
                id: 11,
                title: 'Kachchh'
            }, {
                id: 12,
                title: 'Kheda'
            }, {
                id: 13,
                title: 'Mahesana'
            }, {
                id: 14,
                title: 'Narmada'
            }, {
                id: 15,
                title: 'Navsari'
            }, {
                id: 16,
                title: 'Panch Mahals'
            }, {
                id: 17,
                title: 'Patan'
            }, {
                id: 18,
                title: 'Porbandar'
            }, {
                id: 19,
                title: 'Rajkot'
            }, {
                id: 20,
                title: 'Sabar Kantha'
            }, {
                id: 21,
                title: 'Surat'
            }, {
                id: 22,
                title: 'Surendranagar'
            }, {
                id: 23,
                title: 'Tapi'
            }, {
                id: 24,
                title: 'The Dangs'
            }, {
                id: 25,
                title: 'Vadodara'
            }, {
                id: 26,
                title: 'Valsad'
            }, 
            ]
        },{
        id: 8,
        title: 'Haryana',
        subs: [
            {
                id: 1,
                title: 'Ambala'
            },{
                id: 2,
                title: 'Bhiwani'
            }, {
                id: 3,
                title: 'Faridabad'
            }, {
                id: 4,
                title: 'Fatehabad'
            }, {
                id: 5,
                title: 'Gurgaon'
            }, {
                id: 6,
                title: 'Hisar'
            }, {
                id: 7,
                title: 'Jhajjar'
            }, {
                id: 8,
                title: 'Jind'
            }, {
                id: 9,
                title: 'Kaithal'
            }, {
                id: 10,
                title: 'Karnal'
            }, {
                id: 11,
                title: 'Kurukshetra'
            }, {
                id: 12,
                title: 'Mahendragarh'
            }, {
                id: 13,
                title: 'Mewat'
            }, {
                id: 14,
                title: 'Palwal'
            }, {
                id: 15,
                title: 'Panchkula'
            }, {
                id: 16,
                title: 'Panipat'
            }, {
                id: 17,
                title: 'Rewari'
            }, {
                id: 18,
                title: 'Rohtak'
            }, {
                id: 19,
                title: 'Sirsa'
            }, {
                id: 20,
                title: 'Sonipat'
            }, {
                id: 21,
                title: 'Yamunanagar'
            }, 
            ]
        },{
        id: 9,
        title: 'Himachal Pradesh',
        subs: [
            {
                id: 1,
                title: 'Bilaspur'
            }, {
                id: 2,
                title: 'Chamba'
            }, {
                id: 3,
                title: 'Hamirpur'
            }, {
                id: 4,
                title: 'Kangra'
            }, {
                id: 5,
                title: 'Kinnaur'
            }, {
                id: 6,
                title: 'Kullu'
            },{
                id: 7,
                title: 'Lahul & Spiti'
            },  {
                id: 8,
                title: 'Mandi'
            }, {
                id: 9,
                title: 'Shimla'
            }, {
                id: 10,
                title: 'Sirmaur'
            }, {
                id: 11,
                title: 'Solan'
            }, {
                id: 12,
                title: 'Una'
            }, 
        ]
    }, {
        id: 10,
        title: 'Jammu and Kashmir',
        subs: [
            {
                id: 1,
                title: 'Anantnag'
            }, {
                id: 2,
                title: 'Badgam'
            }, {
                id: 3,
                title: 'Bandipore'
            }, {
                id: 4,
                title: 'Baramula'
            }, {
                id: 5,
                title: 'Doda'
            }, {
                id: 6,
                title: 'Ganderbal'
            }, {
                id: 7,
                title: 'Jammu'
            }, 
            {
                id: 8,
                title: 'Kargil'
            }, 
            {
                id: 9,
                title: 'Kathua'
            }, {
                id: 10,
                title: 'Kishtwar'
            }, {
                id: 11,
                title: 'Kulgam'
            }, {
                id: 12,
                title: 'Kupwara'
            }, {
                id: 13,
                title: 'Leh'
            }, {
                id: 14,
                title: 'Pulwama'
            }, {
                id: 15,
                title: 'punch'
            }, {
                id: 16,
                title: 'Rajouri'
            }, {
                id: 17,
                title: 'Ramban'
            }, {
                id: 18,
                title: 'Reasi'
            },{
                id: 12,
                title: 'Samba'
            }, {
                id: 13,
                title: 'Shupiyan'
            }, {
                id: 14,
                title: 'Srinagar'
            }, {
                id: 15,
                title: 'Udhampur'
            },
            ]
        },{
        id: 11,
        title: 'Jharkhand',
        subs: [
            {
                id: 1,
                title: 'Bokaro'
            }, {
                id: 2,
                title: 'Chatra'
            },{
                id:3,
                title: 'Deoghar'
            },{
                id: 4,
                title: 'Dumka'
            },{
                id: 5,
                title: 'Garhwa'
            },{
                id: 6,
                title: 'Giridih'
            },{
                id: 7,
                title: 'Godda'
            },{
                id: 8,
                title: 'Gumla'
            },{
                id: 9,
                title: 'Hazaribagh'
            },{
                id: 10,
                title: 'Jamtara'
            },{
                id: 11,
                title: 'Khunti'
            },{
                id: 12,
                title: 'Kodarma'
            },{
                id: 13,
                title: 'Latehar'
            },{
                id: 14,
                title: 'Lohardaga'
            },{
                id: 15,
                title: 'Pakur'
            },{
                id: 16,
                title: 'Palamu'
            },{
                id: 17,
                title: 'Pashchimi Singhbhum'
            },{
                id: 18,
                title: 'Purbi Singhbhum'
            },{
                id: 19,
                title: 'Ramgarh'
            },{
                id: 20,
                title: 'Ranchi'
            },{
                id: 21,
                title: 'Sahibganj'
            },{
                id: 22,
                title: 'Saraikela-Kharsawan'
            },{
                id: 23,
                title: 'Simdega'
            },
            ]
        },{
        id: 12,
        title: 'Karnataka',
        subs: [
            {
                id: 1,
                title: 'Bagalkot'
            }, {
                id: 2,
                title: 'Bangalore'
            }, {
                id: 3,
                title: 'Bangalore Rural'
            }, {
                id: 4,
                title: 'Belgaum'
            }, {
                id: 5,
                title: 'Bellary'
            }, {
                id: 6,
                title: 'Bidar'
            }, {
                id: 7,
                title: 'Bijapur'
            }, {
                id: 8,
                title: 'Chamarajanagar'
            }, {
                id: 9,
                title: 'Chikkaballapura'
            }, {
                id: 10,
                title: 'Chikmagalur'
            }, {
                id: 11,
                title: 'Chitradurga'
            }, {
                id: 12,
                title: 'Dakshina Kannada'
            }, {
                id: 13,
                title: 'Davanagere'
            }, {
                id: 14,
                title: 'Dharwad'
            }, {
                id: 15,
                title: 'Gadag'
            }, {
                id: 16,
                title: 'Gulbarga'
            }, {
                id: 17,
                title: 'Hassan'
            }, {
                id: 18,
                title: 'Haveri'
            }, {
                id: 19,
                title: 'Kodagu'
            }, {
                id: 20,
                title: 'Kolar'
            }, {
                id: 21,
                title: 'Koppal'
            }, {
                id: 22,
                title: 'Mandya'
            }, {
                id: 23,
                title: 'Mysore'
            }, {
                id: 24,
                title: 'Raichur'
            }, {
                id: 25,
                title: 'Ramanagara'
            }, {
                id: 26,
                title: 'Shimoga'
            }, {
                id: 27,
                title: 'Tumkur'
            }, {
                id: 28,
                title: 'Udupi'
            }, {
                id: 29,
                title: 'Uttara Kannada'
            }, {
                id: 30,
                title: 'Yadgir'
            },
            ]
            },{
        id: 13,
        title: 'Kerala',
        subs: [
            {
                id: 1,
                title: 'Alappuzha'
            }, {
                id: 2,
                title: 'Ernakulam'
            },{
                id: 3,
                title: 'Idukki'
            },  {
                id: 4,
                title: 'Kannur'
            }, {
                id: 5,
                title: 'Kasaragod'
            }, {
                id: 6,
                title: 'Kollam'
            }, {
                id: 7,
                title: 'Kottayam'
            }, {
                id: 8,
                title: 'Kozhikode'
            }, {
                id: 9,
                title: 'Malappuram'
            }, {
                id: 10,
                title: 'Palakkad'
            }, {
                id: 11,
                title: 'Pathanamthitta'
            }, {
                id: 12,
                title: 'Thiruvananthapuram'
            }, {
                id: 13,
                title: 'Thrissur'
            }, {
                id: 14,
                title: 'Wayanad'
            }, 
            ]
        },

        { 
        id:14,
        title: 'Madhya Pradesh',
        subs: [
            {
                id:1,
                title: 'Indore'
            }, {
                id:2,
                title: 'Bhopal'
            }, {
                id:3,
                title: 'Jabalpur'
            }, {
                id:4,
                title:'Gwalior'
 
            },
             {
                id:5,
                title:'Ujjain'
 
            },
             {
                id:6,
                title:'Sagar'
 
            },
             {
                id:7,
                title:'Dewas'
 
            },
             {
                id:8,
                title:'Satna'
 
            },
             {
                id:9,
                title:'Ratlam'
 
            },
             {
                id:10,
                title:'Rewa'
 
            },
             {
                id:11,
                title:'Murwara'
 
            },
             {
                id:12,
                title:'Singrauli'
 
            },
             {
                id:13,
                title:'Burhanput'
 
            },
            {
                id:14,
                title:'Khandwa'
 
            },
             {
                id:15,
                title:'Bhind'
 
            },
            {
                id:16,
                title:'Chhindwara'
 
            },
             {
                id:17,
                title:'Guna'
 
            },
            {
                id:18,
                title:'Shivpuri'
 
            },
            {
                id:19,
                title:'Vidisha'
 
            },
            {
                id:20,
                title:'Chhatarpur'
 
            },
            {
                id:21,
                title:'Damoh'
 
            },
            {
                id:22,
                title:'Mandsaur'
 
            },
            {
                id:23,
                title:'Khargone'
 
            },
            {
                id:24,
                title:'Neemuch'
 
            },
            {
                id:25,
                title:'pithampur'
 
            },
            {
                id:26,
                title:'Hoshangabad'
 
            },
            {
                id:27,
                title:'itarsi'
 
            }, {
                id:28,
                title:'Sehore'
 
            },
            {
                id:29,
                title:'Betul'
 
            },
            {
                id:30,
                title:'Seoni'
 
            },
            {
                id:31,
                title:'Datia'
 
            },
            {
                id:32,
                title:'Nagda'
 
            },
        ]
    }, { id:15,
           title: 'Maharashtra',
         subs: [
               {
                id:1,
                title: 'Mumbai'
            }, {
                id:2,
                title:'Pune'
 
            },
             {
                id:3,
                title:'Nagpur'
 
            },
             {
                id:4,
                title:'Nashik'
 
            },
             {
                id:5,
                title:'Vasai-Virar'
 
            },
             {
                id:6,
                title:'Aurangabad'
 
            },
             {
                id:7,
                title:'Solapur'
 
            },
             {
                id:8,
                title:'Dhule'
 
            },
             {
                id:9,
                title:'Amaravati'
 
            },
            
             {
                id:10,
                title:'malegon'
 
            },
            
             {
                id:11,
                title:'kolhapur'
 
            },
             {
                id:12,
                title:'Nanded'
 
            },
             {
                id:13,
                title:'Sangli'
 
            },
            {
                id:14,
                title:'Thane'
 
            },
             {
                id:15,
                title:'Akola'
 
            },
            {
                id:16,
                title:'Latur'
 
            },
             {
                id:17,
                title:'jalgaon'
 
            },
            {
                id:18,
                title:'Ahemadnagar'
 
            },
            {
                id:19,
                title:'Kolhapur'
 
            },
            {
                id:20,
                title:'Chandrapur'
 
            },
            {
                id:21,
                title:'Parbhani'
 
            },
            {
                id:22,
                title:'Jalna'
 
            },
            {
                id:23,
                title:'Panvel'
 
            },
            {
                id:24,
                title:'Satara'
 
            },
            {
                id:25,
                title:'Beed'
 
            },
            {
                id:26,
                title:'Yavatmal'
 
            },
            {
                id:27,
                title:'Gondia'
 
            }, {
                id:28,
                title:'Osmanbad'
 
            },
            {
                id:29,
                title:'Nandurbar'
 
            },
            {
                id:30,
                title:'Wardha'
 
            },
            {
                id:31,
                title:'Latur'
 
            },
        ]
    }, { id:16,
           title: 'Manipur',
         subs: [
               {
                id:1,
                title: 'Bishnupur'
            }, {
                id:2,
                title:'Thoubal'
 
            },
             {
                id:3,
                title:'Imphal east'
 
            },
             {
                id:4,
                title:'Imphal west'
 
            },
             {
                id:5,
                title:'senapati'
 
            },
             {
                id:6,
                title:'Ukhrul'
 
            },
             {
                id:7,
                title:'Chandel'
 
            },
             {
                id:8,
                title:'Churachandpur'
 
            },
             {
                id:9,
                title:'Tamenglong'
 
            },
            
             {
                id:10,
                title:'jiribam'
 
            },
            
             {
                id:11,
                title:'Kangpokpi'
 
            },
             {
                id:12,
                title:'kakching'
 
            },
             {
                id:13,
                title:'Tengnoupal'
 
            },
             {
                id:14,
                title:'Kamjong'
 
            },
             {
                id:15,
                title:'Noney'
 
            },
             {
                id:16,
                title:'Pherzawl'
 
            },
              
     ]

 }, {      id:17,
           title: 'Meghalaya',
           subs: [
               {
                id:1,
                title: 'Baghmara'
            }, {
                id:2,
                title:'Cherrapunjee'
 
            },
             {
                id:3,
                title:'Jowai'
 
            },
             {
                id:4,
                title:'Lawsohtum'
 
            },
             {
                id:5,
                title:'Madanriting'
 
            },
             {
                id:6,
                title:'Mairang'
 
            },
             {
                id:7,
                title:'Mawlai'
 
            },
             {
                id:8,
                title:'Mawpat'
 
            },
             {
                id:9,
                title:'Nongkseh'
 
            },
            
             {
                id:10,
                title:'Nongmynsong'
 
            },
            
             {
                id:11,
                title:'Nongpoh'
 
            },
             {
                id:12,
                title:'Nongstoin'
 
            },
             {
                id:13,
                title:'Nongthymmai'
 
            },  {
                id:14,
                title:'pynthormukhrah'
 
            },
            {
                id:15,
                title:'Resubelpara'
 
            },

            {
                id:16,
                title:'Shillong'
 
            },
            {
                id:17,
                title:'Tura'
 
            },

            {
                id:18,
                title:'Umlyngka'
 
            },

            {
                id:19,
                title:'Umpling'
 
            },
            {
                id:20,
                title:'Umroi'
 
            },{
                id:21,
                title:'Williamnagar'
 
            },



        ]
    }, 
    {    id:18,
         title: 'Mizoram',
         subs: [
               {
                id:1,
                title: 'Aizawl'
            }, {
                id:2,
                title:'Bairabi'
 
            },
             {
                id:3,
                title:'Biate'
 
            },
             {
                id:4,
                title:'Champhal'
 
            },
             {
                id:5,
                title:'Darlawn'
 
            },
             {
                id:6,
                title:'Hnahthial'
 
            },
             {
                id:7,
                title:'Khawhal'
 
            },
             {
                id:8,
                title:'Khawzawl'
 
            },
             {
                id:9,
                title:'Kolasib'
 
            },
            
             {
                id:10,
                title:'lawngtlai'
 
            },
            
             {
                id:11,
                title:'lengpui'
 
            },
             {
                id:12,
                title:'Lunglei'
 
            },
             {
                id:13,
                title:'Mamit'
 
            },  {
                id:14,
                title:'N.Kawnpul'
 
            },
            {
                id:15,
                title:'North Vanlaiphai'
 
            },

            {
                id:16,
                title:'Saiha'
 
            },
            {
                id:17,
                title:'Sairang'
 
            },

            {
                id:18,
                title:'Saitual'
 
            },

            {
                id:19,
                title:'serchhip'
 
            },
            {
                id:20,
                title:'Thenzawl'
 
            },{
                id:21,
                title:'Tlabung'
 
            },{
                id:22,
                title:'vairengte'
 
            },{
                id:23,
                title:'Zawlnuam'
 
            },

       ]
    },
  


     {
        id: 19,
        title: 'Nagaland',
        subs: [
            {
                id: 190,
                title: 'Kohima'
            }, {
                id: 191,
                title: 'Dimapur'
            }, {
                id: 192,
                title: 'Mokokchung'
            }, {
                id: 193,
                title: 'Wokha'
            }, {
                id: 194,
                title: 'Tuensang'
            }, {
                id: 195,
                title: 'Zunheboto'
            }, {
                id: 196,
                title: 'Mon Village'
            } 
        ]
    },
    {
        id: 20,
        title: 'Odisa',
        subs: [
            {
                id: 200,
                title: 'Anugal'   
            }, {
                id: 201,
                title: 'Balangir'
            }, {
                id: 202,
                title: 'Baleshwar'
            }, {
                id: 203,
                title: 'Baudh'
            }, {
                id: 204,
                title: 'Bhadrak'
            }, {
                id: 205,
                title: 'Cuttack'
            }, {
                id: 206,
                title: 'Debagarh'
            }, {
                id: 207,
                title: 'Gajapati'
            }, {
                id: 208,
                title: 'Ganjam'
            }, {
                id: 209,
                title: 'Jagatsinghapore'
            }, {
                id: 2010,
                title: 'Jajapur'
            }, {
                id: 2011,
                title: 'Jharsuguda'
            }, {
                id: 2012,
                title: 'Kalahandi'
            }, {
                id: 2013,
                title: 'Kandhamal'
            }, {
                id: 2014,
                title: 'Kendrapara'
            }, {
                id: 2015,
                title: 'Kendujhar'
            }, {
                id: 2016,
                title: 'Malkangiri'
            }, {
                id: 2017,
                title: 'Mayurbhanj'
            }
        ]
    },
    {

        id: 21,
        title: 'punjab',
        subs: [
            {
                id: 210,
                title: 'Amritsar'
            }, {
                id: 211,
                title: 'Barnala'
            }, {
                id: 212,
                title: 'Bathinda'
            }, {
                id: 213,
                title: 'Faridkot'
            }, {
                id: 214,
                title: 'Fatehghar Sahib'
            }, {
                id: 215,
                title: 'Firozpur'
            }, {
                id: 216,
                title: 'Gurdaspur'
            }, {
                id: 217,
                title: 'Hoshiarpur'
            }, {
                id: 218,
                title: 'Jalandhar'
            }, {
                id: 219,
                title: 'Kapurthala'
            }, {
                id: 2110,
                title: 'Ludhiana'
            }, {
                id: 2111,
                title: 'Mansa'
            }, {
                id: 2112,
                title: 'Moga'
            }, {
                id: 2113,
                title: 'Muktsar'
            }, {
                id: 2114,
                title: 'Pathakot'
            }, {
                id: 2115,
                title: 'Patiala'
            }, {
                id: 2116,
                title: 'Rupnagar'
            }, {
                id: 2117,
                title: 'Sahibzada Ajit singh Nagar'
            }, {
                id: 2118,
                title: 'Sangrur'
            }, {
                id: 2119,
                title: 'Shahid bhagad singh Nagar'
            }, {
                id: 2120,
                title: 'Tarn Taran'
            }
        ]
    },
    {
        id: 22,
        title: 'Rajasthan',
        subs:[
            {
                id: 220,
                title: 'ajmer'
            }, {
                id: 221,
                title: 'Alwar'
            }, {
                id: 222,
                title: 'Banswara'
            }, {
                id: 223,
                title: 'Baran'
            }, {
                id: 224,
                title: 'Barmer'
            }, {
                id: 225,
                title: 'Bharatpur'
            }, {
                id: 226,
                title: 'Bhilwara'
            }, {
                id: 227,
                title: 'Bikanar'
            }, {
                id: 228,
                title: 'Bundi'
            }, {
                id: 229,
                title: 'Chittaurgarh'
            }, {
                id: 2210,
                title: 'Churu'
            }, {
                id: 2211,
                title: 'Dausa'
            }, {
                id: 2212,
                title: 'Dhaulpur'
            }, {
                id: 2213,
                title: 'Dungarpur'
            }, {
                id: 2214,
                title: 'Ganaganagar'
            }, {
                id: 2215,
                title: 'Hanumangarh'
            }, {
                id: 2216,
                title: 'Jaipur'
            }, {
                id: 2217,
                title: 'Jaisalmer'
            }, {
                id: 2218,
                title: 'Jalor'
            }, {
                id: 2219,
                title: 'Jhalawar'
            }, {
                id: 2220,
                title: 'Jhunjhunu'
            }, {
                id: 2221,
                title: 'Jodhpur'
            }, {
                id: 2222,
                title: 'Karauli'
            }, {
                id: 2223,
                title: 'Nagaur'
            }, {
                id: 2224,
                title: 'Pali'
            }, {
                id: 2225,
                title: 'Pratapgarh'
            }, {
                id: 2226,
                title: 'Rajsamand'
            }, {
                id: 2227,
                title: 'sawali Madhopur'
            }, {
                id: 2228,
                title: 'sikar'
            }, {
                id: 2229,
                title: 'sirohi'
            }, {
                id: 2230,
                title: 'Tonk'
            }, {
                id: 2231,
                title: 'Udaipur'
            }
        ]
    },
    {
        id: 23,
        title: 'Sikkim',
        subs: [
            {
                id: 230,
                title: 'Gangtok'
            }, {
                id: 231,
                title: 'Jorethang'
            }, {
                id: 230,
                title: 'Namchi'
            }, {
                id: 230,
                title: 'Rangpo'
            }
        ]
    },
    {
        id: 24,
        title: 'Tamilnadu',
        subs: [
            {
                id: 1,
                title: 'Chennai'
            }, {
                id: 2,
                title: 'Coimbatore'
            }, {
                id: 3,
                title: 'Madurai'
            },
             {
                id: 4,
                title: 'Trichy'
            }, {
                id: 5,
                title: 'Tiruppur'
            }, {
                id: 6,
                title: 'salem'
            }, {
                id: 7,
                title: 'Erode'
            }, {
                id: 8,
                title: 'Tirunelveli'
            }, {
                id: 9,
                title: 'vellore'
            }, {
                id: 10,
                title: 'Thoothukkudi'
            }, {
                id: 11,
                title: 'Nagercoil'
            }, {
                id: 12,
                title: 'Thanjavur'
            }, {
                id: 13,
                title: 'Dindigul'
            }, {
                id: 14,
                title: 'Cuddalore'
            }, {
                id: 15,
                title: 'kanchipuram'
            }, {
                id: 14,
                title: 'Tiruvannamalai'
            }, {
                id: 15,
                title: 'kumbakonam'
            }, {
                id: 16,
                title: 'Rajapalayam'
            }, {
                id: 17,
                title: 'Pudukottai'
            }, {
                id: 18,
                title: 'Hosur'
            }, {
                id: 19,
                title: 'Ambur'
            }, {
                id: 21,
                title: 'Karaikudi'
            }, {
                id: 22,
                title: 'Neyveli'
            }, {
                id: 23,
                title: 'Nagapattinam'
            }, 

        ]
    }


];
var comboTree1;

jQuery(document).ready(function($) {

        comboTree1 = $('#c_state').comboTree({
            source : city,
                isMultiple: true
        });

        
});


                                     
