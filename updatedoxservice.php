<?php 
include 'header.php';
?>
<script>
    

    function applyImage(inputField, target, errorTarget) {

        $(errorTarget).html("");

        var input = inputField;
        var url = $(inputField).val();
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(target).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            $(errorTarget).html("invalid file");
        }

    }

    $(function() {
        $("#ser-bpanimg").change(function() {
            applyImage(this, "#bpanImg", "#ppan_status");
        });
        $("#ser-baddimg").change(function() {
            applyImage(this, "#baddimg", "#addr_status");
        });
        $("#ser-bcheimg").change(function() {
            applyImage(this, "#bcanimg", "#cheque_status");
        });
        $("#b-serve-pan").change(function(){
            applyImage(this, "#serbusi-panimgavatar", "#buis-panimgstatus")
        });
        $('#ser-busaahd').change(function(){
            applyImage(this, "#serbusi-aadhimgavatar", "#serbusi-aadhstatus");
        });
        $('#ser-busdigi').change(function(){
            applyImage(this, "#ser-busdigiimgava", "#serbus-dig_status");
        });
        $('#ind-ser-cenbtn').change(function(){
            applyImage(this, "#ind-ser-cenimg", "#ind-ser-censtat");
        });
        $('#ind-ser-licbtn').change(function(){
            applyImage(this, "#ind-ser-licenimg", "#ind-ser-licstat");
        });
        $('#ind-ser-proobtn').change(function(){
            applyImage(this, "#ind-ser-prooimg", "#ind-ser-prostat");
        });
        $('#ind-ser-photobtn').change(function(){
            applyImage(this, "#ind-ser-photoimg", "#ind-ser-photostat");
        });
    });
</script>

<!-- start section -->
 <div class="section white-backgorund">
            <div class="container">
                <div class="row">
                    
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12 text-left">
                                <h2 class="title" style="margin-top: 10px;">My Account</h2>
                            </div><!-- end col -->
                        </div><!-- end row -->
                        <hr class="spacer-5"><hr class="spacer-20 no-border">
                        <div class="row" >
                            <div class="col-sm-2 text-left" >
                                <ul style="list-style-type: none;padding-left: 0px;">
                                    <li><a href="profille.php">Profile</a></li>
                                    <li><a href="updateacount.php">Upgrade Account</a></li>
                                    <li><a href="updatedoxservice.php"><strong>Update Document Service</strong></a></li>
                                    <li><a href="updatedox.php"><strong>Update Document Product</strong></a></li>
                                </ul>
                            </div>
                            <div class="col-sm-10 text-left" style="border-left: 1px solid rgba(0, 0, 0, 0.1);" >
                                <h4 style="margin-top: 0px;">Update Document Services</h4>
                                <label>Post Service Details</label>
                                <ul>
                                <li style="list-style:none;">
                                    <input type="radio" name="p_s_type" class="form-check-input" value="1" id="p_s_typeIndiv">
                                    Indivdual
                                    <input type="radio" name="p_s_type" value="2" class="form-check-input" checked="" id="p_s_typeCompany">
                                    Company
                                    
                                </li>
                                </ul>
                                <div class="row" style="text-align: center;">
                                <div id="bussinessTab" class="col-sm-4" style="border: 1px solid rgba(0, 0, 0, 0.1);height: 200px;width:300px ;margin: 10px 5px;padding: 0px;" >
                                    <h5 style="background-color: rgba(0, 0, 0, 0.1); margin: 0px;padding: 10px;">Bussiness Details</h5>
                                    <p>You need to provide your GSTIN , TAN and other bussiness information</p>
                                    <a class="btn btn-default round btn-md" data-toggle="modal" data-target="#bussinessPopup">Add Details</a>
                                </div>
                                <div id="individualTab" class="col-sm-4 hidden" style="border: 1px solid rgba(0, 0, 0, 0.1);height: 200px;width:300px ;margin: 10px 5px;padding: 0px;" >
                                    <h5 style="background-color: rgba(0, 0, 0, 0.1); margin: 0px;padding: 10px;">Individual Details</h5>
                                    <p>You need to provide your details to post services.</p>
                                    <a class="btn btn-default round btn-md" data-toggle="modal" data-target="#indivPopup">Add Details</a>
                                </div>
                                <div class="col-sm-4" style="border: 1px solid rgba(0, 0, 0, 0.1);height: 200px;width:300px ;margin: 10px 5px;padding: 0px;">
                                    <h5 style="background-color: rgba(0, 0, 0, 0.1);margin: 0px; padding: 10px;">Bank Details</h5>
                                    <p>we need your bank account details and KYC documents to verify your bank account</p>
                                    <a class="btn btn-default round btn-md" data-toggle="modal" data-target="#bankPopup">Add Details</a>
                                </div>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 

        <!-- Modal starts here -->
        <div class="modal fade" id="bussinessPopup" tabindex="-1" role="dialog" aria-labelledby="bussinessDetails" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="bussinessDetails">Business Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form id="businessForm" method="get" action="">
                        <div class="form-group">
                           <div class="form-group">
                                <label for="b_company">Company Name</label>
                                <input type="text" class="form-control" id="b_company" placeholder="Company Name" name="b_company">
                            </div>
                            <!-- <div class="form-group">
                                <label for="b_type">Business Type</label>
                                <input type="radio" name="b_type" id="b_type" value="option1"> Wholesaler 
                                <input type="radio" name="b_type" id="b_type" value="option2"> Retailer 
                                <input type="radio" name="b_type" id="b_type" value="option3"> Distributor 
                                <input type="radio" name="b_type" id="b_type" value="option4"> Manufacturer 
                                <input type="radio" name="b_type" id="b_type" value="option5"> Others 
                            </div> -->
                            <div class="form-group">
                                <label for="b_gstin">GSTIN Number</label>
                                <input type="text" class="form-control" id="b_gstin" placeholder="GSTIN Number" name="b_gstin">
                            </div>
                            <div class="form-group">
                                <label for="b_address">Company Address</label>
                                <input class="form-control" id="c_address" placeholder="Company Address" name="b_address">
                                <input type='hidden' id='b_lat'>
                                <input type='hidden' id='b_lng'>
                            </div>
                            <div class="form-group">
                                <label for="b_address">Billing Address</label>
                                <div class="row">
                                <div class="col-md-6">
                                <input class="form-control bill-add" id="c_billaddress" placeholder="Billing Address" name="c_address">
                                </div>
                                <div class="col-md-6">
                                <input type="button" id="sameas"> Click here if same as company's Address
                                </div>
                                </div>
                                <input type='hidden' id='b_lat'>
                                <input type='hidden' id='b_lng'>
                            </div>
                            <div class="form-group">
                                <label for="b_pan_no">PAN Number</label>
                                <input type="text" class="form-control" id="b_pan_no" placeholder="PAN Number" name="b_pan_no">
                            </div> 
                            <div class="form-group">
                                <label for="exampleInputEmail1">PAN Card Image</label>
                                
                                <figure class="user-avatar medium">
                                    <img class='panImg img-circle' alt="avatar" id="serbusi-panimgavatar" src='img/avatar.png' style='height:65px;width:65px' >
                                    <button id="b-ser-panimage" class="work-up-btn" onclick="document.getElementById('b-serve-pan').click(); return false;">Upload</button>
                                    <input type='file' id="b-serve-pan" onchange="readURL(this);" style="visibility: hidden;">
                                    <span id='buis-panimgstatus'></span>
                                </figure>
                            </div>
                            <div class="form-group">
                                <label for="b_tan_no">TAN Number</label>
                                <input type="text" class="form-control" id="b_tan_no" placeholder="TAN Number" name="b_tan_no">
                            </div>
                            <div class="form-group">
                                <label for="b_aadhar">Aadhar Number</label>
                                <input type="text" class="form-control" placeholder="Aadhar Number" name="b_aadhar" id="b_aadhar">
                            </div>
                            <div class="row service-proof">
                                <div class="col-md-6">  
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Aadhar Image</label>
                                        
                                        <figure class="user-avatar medium">
                                            <img class='aadharImg img-circle' id="serbusi-aadhimgavatar" alt="avatar" src='img/avatar.png' style='height:65px;width:65px' >
                                            <button id="equipment-photo" class="work-up-btn" onclick="document.getElementById('ser-busaahd').click(); return false;">Upload</button>
                                            <input type='file' id="ser-busaahd" onchange="readURL(this);" style="visibility: hidden;">
                                            <span id='serbusi-aadhstatus'></span>
                                        </figure>
                                        <span class="error" id="aadharUrlHTML"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Digital Image Signature</label>
                                        
                                        <figure class="user-avatar medium">
                                            <img class='digImg img-circle' alt="avatar" id="ser-busdigiimgava" src='img/avatar.png' style='height:65px;width:65px' >
                                            <button id="equipment-photo" class="work-up-btn" onclick="document.getElementById('ser-busdigi').click(); return false;">Upload</button>
                                            <input type='file' id="ser-busdigi" onchange="readURL(this);" style="visibility: hidden;">
                                            <span id='serbus-dig_status'></span>
                                        </figure>
                                        <span class="error" id="digUrlHTML"></span>
                                        <input type="hidden" id="aadharUrl" value="{{Auth::user()->aadhar_image}}">
                                        <input type="hidden" id="panUrl" value="{{Auth::user()->pan_image}}">
                                        <input type="hidden" id="digUrl" value="{{Auth::user()->dig_image}}">
                                        <input type="hidden" id="ppanUrl" value="{{Auth::user()->p_pan_image}}">
                                        <input type="hidden" id="addrUrl" value="{{Auth::user()->address_img}}">
                                        <input type="hidden" id="ccUrl" value="{{Auth::user()->cheque_img}}">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <input type="button" class="btn btn-primary" name="submit" id="business-service-btn" value="Save Changes">
                                        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                        <p id="bus-ser-errmsg">-</p>
                            </div>
                                    
                        </div>
                    </form>
                  </div>
                  
                </div>
              </div>
            </div>
            <div class="modal fade" id="indivPopup" tabindex="-1" role="dialog" aria-labelledby="bussinessDetails" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="indivDetails">Individual Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form id="individualForm" action="" method="get">
                                <div class="form-group">
                                    <label for="i_type">Category Type</label>
                                    <select class="form-control" id="i_type" name="i_type">
                                       <option value="">Choose any Options</option>
                                       <option value="1">Choosed</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="i_stype">Sub-Category Type</label>
                                    <select class="form-control" id="i_stype" name="i_stype">
                                        <option value="">Choose any Options</option>
                                        <option value="1">Choosed</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="i_name">Name :</label>
                                    <input type="text" class="form-control" id="i_name" placeholder="Name" name="i_name">
                                </div>

                                <div class="form-group">
                                    <label for="i_address">Address :</label>
                                    <input type="text" class="form-control" id="i_address" placeholder="Address" name="i_address">
                                </div>
                                <div class="form-group">
                                    <label for="i_cert">Certification if Available :</label>
                                    <img class='poiImg img-circle' alt="avatar" src="img/avatar.png" id="ind-ser-cenimg" style='height:65px;width:65px' >
                                    <button id="equipment-photo" class="work-up-btn" onclick="document.getElementById('ind-ser-cenbtn').click(); return false;">Upload</button>
                                    <input type='file' id="ind-ser-cenbtn" onchange="readURL(this);" style="visibility: hidden;"><p id="ind-ser-censtat"></p>
                                </div>
                                <div class="form-group">
                                    <label for="i_cert">License :</label>
                                    <input type="text" class="form-control" id="i_cert" placeholder="Certification" name="i_cert">
                                    <img class='poiImg img-circle' alt="avatar" src="img/avatar.png" id="ind-ser-licenimg" style='height:65px;width:65px;margin-top: 10px;' >
                                    <button id="equipment-photo" class="work-up-btn" onclick="document.getElementById('ind-ser-licbtn').click(); return false;">Upload</button>
                                    <input type='file' id="ind-ser-licbtn" onchange="readURL(this);" style="visibility: hidden;"><p id="ind-ser-licstat"></p>
                                </div>
                                <div class="row service-proof">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Proof of Identity</label>
                                            
                                            <figure class="user-avatar medium"> 
                                                <img class='poiImg img-circle' alt="avatar" id="ind-ser-prooimg" src="img/avatar.png" style='height:65px;width:65px'>
                                                <button id="equipment-photo" class="work-up-btn" onclick="document.getElementById('ind-ser-proobtn').click(); return false;">Upload</button>
                                                <input type='file' id="ind-ser-proobtn" onchange="readURL(this);" style="visibility: hidden;">
                                                <span id='ind-ser-prostat'></span>
                                            </figure>
                                            <input type='hidden' name='poiUrl'>
                                            <span class="error" id="poiUrlHTML"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="uploadForms">
                                            <label for="email">Upload Photograph</label>
                                            <figure class="user-avatar medium">
                                            
                                            <img class='userImg img-circle' alt="avatar" id="ind-ser-photoimg" src="img/avatar.png" style='height:65px;width:65px' >
                                            <button id="equipment-photo" class="work-up-btn" onclick="document.getElementById('ind-ser-photobtn').click(); return false;">Upload</button>
                                            <input type='file' id="ind-ser-photobtn" onchange="readURL(this);" style="visibility: hidden;"><p id="ind-ser-photostat"></p>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email">Machinery/Work Man/vehicle involved ? :</label>
                                    
                                    <div>
                                        <input type="radio" name="workmanchoose" id="workman_thing_yes" value="yes">
                                        <label for="choose">yes</label>
                                        <input type="radio" name="workmanchoose" id="workman_thing_no" value="no">
                                        <label for="choose">No</label>
                                        <p id="ifhadequip"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div>
                                        <input type="checkbox" id="indi-ser-tandc">
                                        <label for="checkbox2">
                                        I there by agree to the TERMS &amp; CONDITIONS of 7ATARA
                                        </label>
                                        <p id="ser_ind_tandclabel"></p>
                                    </div>
                                </div>
                                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <input type="button" class="btn btn-primary" name="submit" id="ser_ind_btn" value="Save Changes">
                                </div>
                            </form>   
                        </div>
                    </div>
                </div>
              </div>
        <div class="modal fade" id="bankPopup" tabindex="-1" role="dialog" aria-labelledby="bankDetails" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="bankDetails">Bank Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form id="bankForm" method="get" action="">
                        <div class="form-group">
                            <div class="form-group">
                            <label for="ac_name">Account Holder Name</label>
                            <input type="text" class="form-control" id="ac_name" placeholder="Account Holder Name" name="ac_name">
                            </div>
                            <div class="form-group">
                            <label for="ac_no">Account Number</label>
                            <input type="text" class="form-control" id="ac_no" placeholder="Account Number" name="ac_no">
                            </div>
                            <div class="form-group ">
                            <label for="ac_no2">Confirm Account Number</label>
                            <input type="text" class="form-control" id="ac_no2" placeholder="Confirm Account Number" name="ac_no2">
                            </div>
                            <div class="form-group">
                            <label for="ac_b_name">Bank Name</label>
                            <input type="text" class="form-control" id="ac_b_name" placeholder="Bank Name" name="ac_b_name">
                            </div>
                            <div class="form-group">
                            <label for="ac_ifsc">IFSC Number</label>
                            <a class='pull-right' href='https://bankifsccode.com/' target="_blank">Click here to find your IFSC Code</a>
                            <input type="text" class="form-control" id="ac_ifsc" placeholder="IFSC Number" name="ac_ifsc">
                            </div>
                            <!-- <div class="form-group"> -->
                            <h5>KYC Documents</h5>
                           <!--  <label for="ac_btype">Business Type</label>
                            <select class='form-control' id='ac_btype' name="ac_btype">
                            <option value='0'>Wholesaler</option>
                            <option value='1'>Retailer</option>
                            <option value='2'>Manufacturer</option>
                            <option value='3'>Distributor</option>
                            </select> -->
                            <!-- </div> -->
                            <div class="form-group">
                            <label for="ac_p_pan">Personal PAN Card Number</label>
                            <input type="text" class="form-control" id="ac_p_pan" placeholder="Personal PAN Number" name="ac_p_pan">
                            </div>
                            <div class="form-group">
                            <label for="exampleInputEmail1">Personal PAN Card Image</label>
                           
                            <figure class="user-avatar medium">
                                <img class='ppanImg img-circle' id="bpanImg" alt="avatar" src='img/avatar.png' style='height:65px;width:65px' >
                                <button id="ser-bpan" class="work-up-btn" onclick="document.getElementById('ser-bpanimg').click(); return false;">Upload</button>
                                <input type='file' id="ser-bpanimg" onchange="readURL(this);" style="visibility: hidden;">
                                <span id='ppan_status'></span>
                            </figure>
                            <span class="error" id="ppanUrlHTML"></span>
                            </div>
                            <div class="form-group">
                            <label for="ac_address">Personal Address</label>
                            <input type="text" class="form-control" id="ac_address" placeholder="Personal Address" name="ac_address">
                            </div>
                            <div class="row service-proof">
                                <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Address Proof Image</label>
                                        
                                        <figure class="user-avatar medium">
                                            <img class='addrImg img-circle' id="baddimg" alt="avatar" src='img/avatar.png' style='height:65px;width:65px' >
                                            <button id="equipment-photo" class="work-up-btn" onclick="document.getElementById('ser-baddimg').click(); return false;">Upload</button>
                                            <input type='file' id="ser-baddimg" onchange="readURL(this);" style="visibility: hidden;">
                                            <span id='addr_status'></span>
                                        </figure>
                                        <span class="error" id="addrUrlHTML"></span>
                                        </div>
                                </div>
                                <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Cancelled Cheque Image Proof</label>
                                        
                                        <figure class="user-avatar medium">
                                            <img class='chequeImg img-circle' id="bcanimg" alt="avatar" src='img/avatar.png' style='height:65px;width:65px' >
                                            <button id="equipment-photo" class="work-up-btn" onclick="document.getElementById('ser-bcheimg').click(); return false;">Upload</button>
                                            <input type='file' id="ser-bcheimg" onchange="readURL(this);" style="visibility: hidden;">
                                            <span id='cheque_status'></span>
                                        </figure>
                                        <span class="error" id="ccUrlHTML"></span>
                                        </div>
                                </div>
                                </div>
                            <div class="form-group hidden">
                            <label for="exampleInputEmail1">Note</label>
                            <textarea class="form-control" placeholder="Note"></textarea>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <input type="button" class="btn btn-primary" name="service-indivi" id="service-bank" value="Save Changes">
                                <p id='ser-ban-err'>--</p>
                                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                            </div>
                                    
                        </div>
                    </form>
                  </div>
                  
                </div>
              </div>
            </div>

            <!-- modal end here -->



<?php 
include 'footer.php';
?>