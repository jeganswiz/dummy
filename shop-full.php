<!DOCTYPE html>
<html lang="en">


<head>
    <title>7Atara</title>
    <meta charset="utf-8">
    <meta name="description" content="7ATARA E-Commerce">
    <meta name="author" content="Mello" />
    <meta name="keywords" content="plus, html5, css3, template, ecommerce, e-commerce, bootstrap, responsive, creative" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!--Favicon-->
    <link rel="shortcut icon" href="img/7a-log.ico" type="image/x-icon">
    <link rel="icon" href="img/7a-log.ico" type="image/x-icon">

    <!-- css files -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css" />
    <link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css" />
    <link rel="stylesheet" type="text/css" href="css/animate.css" />
    <link rel="stylesheet" type="text/css" href="css/swiper.css" />

    <!-- this is default skin you can replace that with: dark.css, yellow.css, red.css ect -->
    <link id="pagestyle" rel="stylesheet" type="text/css" href="css/default.css" />

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700i" rel="stylesheet">

</head>
    <!-- <body onscroll="hide()"> -->
<body>
  <!-- start section -->
        <section class="primary-background hidden-xs">
            <div class="container-fluid">
                <div class="row grid-space-0">
                    <div class="col-sm-12">
                        <figure>
                            <a href="category.html">
                                <img src="img/banners/top_banner.jpg" alt=""/>
                            </a>
                        </figure>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section>
        <!-- end section -->

        <!-- start topBar -->





        <div class="middleBar">
            <div class="container">
                <div class="row display-table">
                    <div class="col-sm-2 vertical-align text-left hidden-xs">
                        <a href="javascript:void(0);">
                            <img width="160" src="img/logo.png" alt="" />
                        </a>
                    </div><!-- end col -->
                    <div class="col-sm-8 vertical-align text-center">
                        <form>
                            <div class="row grid-space-1 oneline-form">
                              <div class="col-sm-3">
                                  <select class="form-control input-lg category-box" name="category" >
                                      <option value="all">Categories</option>
                                      <optgroup label="Mens">
                                          <option value="shirts">Shirts</option>
                                          <option value="coats-jackets">Coats & Jackets</option>
                                          <option value="underwear">Underwear</option>
                                          <option value="sunglasses">Sunglasses</option>
                                          <option value="socks">Socks</option>
                                          <option value="belts">Belts</option>
                                      </optgroup>
                                      <optgroup label="Womens">
                                          <option value="bresses">Bresses</option>
                                          <option value="t-shirts">T-shirts</option>
                                          <option value="skirts">Skirts</option>
                                          <option value="jeans">Jeans</option>
                                          <option value="pullover">Pullover</option>
                                      </optgroup>
                                      <option value="kids">Kids</option>
                                      <option value="fashion">Fashion</option>
                                      <optgroup label="Sportwear">
                                          <option value="shoes">Shoes</option>
                                          <option value="bags">Bags</option>
                                          <option value="pants">Pants</option>
                                          <option value="swimwear">Swimwear</option>
                                          <option value="bicycles">Bicycles</option>
                                      </optgroup>
                                      <option value="bags">Bags</option>
                                      <option value="shoes">Shoes</option>
                                      <option value="hoseholds">HoseHolds</option>
                                      <optgroup label="Technology">
                                          <option value="tv">TV</option>
                                          <option value="camera">Camera</option>
                                          <option value="speakers">Speakers</option>
                                          <option value="mobile">Mobile</option>
                                          <option value="pc">PC</option>
                                      </optgroup>
                                  </select>
                              </div>
                                <div class="col-sm-6">
                                    <input type="text" name="keyword" class="form-control input-lg" placeholder="Search">
                                </div><!-- end col -->
                                <!-- end col -->
                                <div class="col-sm-3">
                                    <input type="submit"  class="btn btn-default btn-block btn-lg" value="Search">
                                </div><!-- end col -->
                            </div><!-- end row -->
                        </form>
                    </div><!-- end col -->
                    <div class="topBar">
                      <ul class="topBarNav pull-right">
                        <li class="linkdown">
                            <a href="javascript:void(0);">
                                <i class="fa fa-user fa-2 mr-5"></i>
                                <span class="hidden-xs">

                                    <i class="fa fa-angle-down fa-2 ml-5"></i>
                                </span>
                            </a>
                            <ul class="w-150">
                                <li><a href="login.html">Login</a></li>
                                <li><a href="register.html">Create Account</a></li>
                                <li class="divider"></li>
                                <li><a href="wishlist.html">Wishlist (5)</a></li>
                                <li><a href="cart.html">My Cart</a></li>
                                <li><a href="checkout.html">Checkout</a></li>
                            </ul>
                        </li>
                        <li class="linkdown pull-right">
                            <a href="javascript:void(0);">
                                <i class="fa fa-shopping-basket fa-2 mr-5"></i>
                                <sup class="text-primary2">(3)</sup>
                                <span class="hidden-xs">
                                    <i class="fa fa-angle-down fa-2 ml-5"></i>
                                </span>
                            </a>
                            <ul class="cart w-250">
                                <li>
                                    <div class="cart-items">
                                        <ol class="items">
                                            <li>
                                                <a href="shop-single-product-v1.html" class="product-image">
                                                    <img src="img/products/men_06.jpg" alt="Sample Product ">
                                                </a>
                                                <div class="product-details">
                                                    <div class="close-icon">
                                                        <a href="javascript:void(0);"><i class="fa fa-close"></i></a>
                                                    </div>
                                                    <p class="product-name">
                                                        <a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a>
                                                    </p>
                                                    <strong>1</strong> x <span class="price text-primary">$59.99</span>
                                                </div><!-- end product-details -->
                                            </li><!-- end item -->
                                            <li>
                                                <a href="shop-single-product-v1.html" class="product-image">
                                                    <img src="img/products/shoes_01.jpg" alt="Sample Product ">
                                                </a>
                                                <div class="product-details">
                                                    <div class="close-icon">
                                                        <a href="javascript:void(0);"><i class="fa fa-close"></i></a>
                                                    </div>
                                                    <p class="product-name">
                                                        <a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a>
                                                    </p>
                                                    <strong>1</strong> x <span class="price text-primary">$39.99</span>
                                                </div><!-- end product-details -->
                                            </li><!-- end item -->
                                            <li>
                                                <a href="shop-single-product-v1.html" class="product-image">
                                                    <img src="img/products/bags_07.jpg" alt="Sample Product ">
                                                </a>
                                                <div class="product-details">
                                                    <div class="close-icon">
                                                        <a href="javascript:void(0);"><i class="fa fa-close"></i></a>
                                                    </div>
                                                    <p class="product-name">
                                                        <a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a>
                                                    </p>
                                                    <strong>1</strong> x <span class="price text-primary">$29.99</span>
                                                </div><!-- end product-details -->
                                            </li><!-- end item -->
                                        </ol>
                                    </div>
                                </li>
                                <li>
                                    <div class="cart-footer">
                                        <a href="cart.html" class="pull-left"><i class="fa fa-cart-plus mr-5"></i>View Cart</a>
                                        <a href="checkout.html" class="pull-right"><i class="fa fa-shopping-basket mr-5"></i>Checkout</a>
                                    </div>
                                </li>
                            </ul>
                          </li>
                        </ul>
                      </div>
                    </div><!-- end col -->
                </div><!-- end  row -->
            </div><!-- end container -->
        </div><!-- end middleBar -->

        <!-- start navbar -->
        <div class="navbar yamm navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#navbar-collapse-3" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="javascript:void(0);" class="navbar-brand visible-xs">
                        <img src="img/logo.png" alt="logo">
                    </a>
                </div>
                <div id="navbar-collapse-3" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <!-- Home -->
                        <li class="dropdown active"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Best Deals<i class="fa fa-angle-down ml-5"></i></a>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="home-v1.html">Home - Version 1</a></li>
                                <li><a href="home-v2.html">Home - Version 2</a></li>
                                <li><a href="home-v3.html">Home - Version 3</a></li>
                                <li><a href="home-v4.html">Home - Version 4 <span class="label primary-background">1.1</span></a></li>
                                <li class="active"><a href="home-v5.html">Home - Version 5 <span class="label primary-background">1.1</span></a></li>
                                <li><a href="home-v6.html">Home - Version 6 <span class="label primary-background">1.2</span></a></li>
                                <li><a href="home-v7.html">Home - Version 7 <span class="label primary-background">1.3</span></a></li>
                            </ul><!-- end ul dropdown-menu -->
                        </li><!-- end li dropdown -->
                        <!-- Features -->
                        <li class="dropdown left"><a href="#" data-toggle="dropdown" class="dropdown-toggle">New Deals<i class="fa fa-angle-down ml-5"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="headers.html">Headers</a></li>
                                <li><a href="footers.html">Footers</a></li>
                                <li><a href="sliders.html">Sliders</a></li>
                                <li><a href="typography.html">Typography</a></li>
                                <li><a href="grid.html">Grid</a></li>
                                <li class="divider"></li>
                                <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Dropdown Level 1</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Dropdown Level</a></li>
                                        <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Dropdown Level 2</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0);">Dropdown Level</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul><!-- end ul dropdown-menu -->
                        </li><!-- end li dropdown -->
                        <!-- Pages -->

                        <!-- elements -->

                        <!-- Collections -->
                        <li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Made in India<i class="fa fa-angle-down ml-5"></i></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="yamm-content">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3">
                                                <a href="javascript:void(0);">
                                                    <figure class="zoom-out">
                                                        <img alt="" src="img/banners/collection_01.jpg">
                                                    </figure>
                                                </a>
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <a href="javascript:void(0);">
                                                    <figure class="zoom-in">
                                                        <img alt="" src="img/banners/collection_02.jpg">
                                                    </figure>
                                                </a>
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <a href="javascript:void(0);">
                                                    <figure class="zoom-out">
                                                        <img alt="" src="img/banners/collection_03.JPG">
                                                    </figure>
                                                </a>
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <a href="javascript:void(0);">
                                                    <figure class="zoom-in">
                                                        <img alt="" src="img/banners/collection_04.JPG">
                                                    </figure>
                                                </a>
                                            </div><!-- end col -->
                                        </div><!-- end row -->

                                        <hr class="spacer-20 no-border">

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3">
                                                <h6>Pellentes que nec diam lectus</h6>
                                                <p>Proin pulvinar libero quis auctor pharet ra. Aenean fermentum met us orci, sedf eugiat augue pulvina r vitae. Nulla dolor nisl, molestie nec aliquam vitae, gravida sodals dolor...</p>
                                                <button type="button" class="btn btn-default round btn-md">Read more</button>
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="thumbnail store style1">
                                                    <div class="header">
                                                        <div class="badges">
                                                            <span class="product-badge top left white-backgorund text-primary semi-circle">Sale</span>
                                                            <span class="product-badge top right text-primary">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half-o"></i>
                                                            </span>
                                                        </div>
                                                        <figure class="layer">
                                                            <img src="img/products/men_01.jpg" alt="">
                                                        </figure>
                                                        <div class="icons">
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="caption">
                                                        <h6 class="thin"><a href="javascript:void(0);">Lorem Ipsum dolor sit</a></h6>
                                                        <div class="price">
                                                            <small class="amount off">$68.99</small>
                                                            <span class="amount text-primary">$59.99</span>
                                                        </div>
                                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                                    </div><!-- end caption -->
                                                </div><!-- end thumbnail -->
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="thumbnail store style1">
                                                    <div class="header">
                                                        <div class="badges">
                                                            <span class="product-badge top left white-backgorund text-primary semi-circle">Sale</span>
                                                            <span class="product-badge top right text-primary">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half-o"></i>
                                                            </span>
                                                        </div>
                                                        <figure class="layer">
                                                            <img src="img/products/women_01.jpg" alt="">
                                                        </figure>
                                                        <div class="icons">
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="caption">
                                                        <h6 class="thin"><a href="javascript:void(0);">Lorem Ipsum dolor sit</a></h6>
                                                        <div class="price">
                                                            <small class="amount off">$68.99</small>
                                                            <span class="amount text-primary">$59.99</span>
                                                        </div>
                                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                                    </div><!-- end caption -->
                                                </div><!-- end thumbnail -->
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="thumbnail store style1">
                                                    <div class="header">
                                                        <div class="badges">
                                                            <span class="product-badge top left white-backgorund text-primary semi-circle">Sale</span>
                                                            <span class="product-badge top right text-primary">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half-o"></i>
                                                            </span>
                                                        </div>
                                                        <figure class="layer">
                                                            <img src="img/products/kids_01.jpg" alt="">
                                                        </figure>
                                                        <div class="icons">
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="caption">
                                                        <h6 class="thin"><a href="javascript:void(0);">Lorem Ipsum dolor sit</a></h6>
                                                        <div class="price">
                                                            <small class="amount off">$68.99</small>
                                                            <span class="amount text-primary">$59.99</span>
                                                        </div>
                                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                                    </div><!-- end caption -->
                                                </div><!-- end thumbnail -->
                                            </div><!-- end col -->
                                        </div><!-- end row -->
                                    </div><!-- end yamm-content -->
                                </li><!-- end li -->
                            </ul><!-- end dropdown-menu -->
                        </li><!-- end dropdown -->
                        <li class="dropdown left"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Shop by category<i class="fa fa-angle-down ml-5"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="headers.html">Headers</a></li>
                                <li><a href="footers.html">Footers</a></li>
                                <li><a href="sliders.html">Sliders</a></li>
                                <li><a href="typography.html">Typography</a></li>
                                <li><a href="grid.html">Grid</a></li>
                                <li class="divider"></li>
                                <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Dropdown Level 1</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Dropdown Level</a></li>
                                        <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Dropdown Level 2</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0);">Dropdown Level</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul><!-- end ul dropdown-menu -->
                        </li><!-- end li dropdown -->
                        <li class="dropdown left"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Popular categories<i class="fa fa-angle-down ml-5"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="headers.html">Headers</a></li>
                                <li><a href="footers.html">Footers</a></li>
                                <li><a href="sliders.html">Sliders</a></li>
                                <li><a href="typography.html">Typography</a></li>
                                <li><a href="grid.html">Grid</a></li>
                                <li class="divider"></li>
                                <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Dropdown Level 1</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Dropdown Level</a></li>
                                        <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Dropdown Level 2</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0);">Dropdown Level</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul><!-- end ul dropdown-menu -->
                        </li><!-- end li dropdown -->

                    </ul><!-- end navbar-nav -->
                    <ul class="nav navbar-nav navbar-right">
                      <li class="dropdown right "><a href="javascript:void(0);" id="unique">Sell Products</a></li>
                      <li class="dropdown right "><a href="javascript:void(0);" id="unique">Post Services</a></li>
                    </ul>
                  <!-- end navbar-right -->
                </div><!-- end navbar collapse -->
            </div><!-- end container -->
        </div><!-- end navbar -->    
        
        <!-- start section -->
        <section class="section white-backgorund">
            <div class="container-fluid padding">
                <div class="row">
                    <!-- start sidebar -->
                    <div class="col-sm-3">
                        
                        <div class="widget">
                            <h6 class="subtitle">Categories</h6>
                            
                            <ul class="list list-unstyled">
                                <li>
                                    <div class="checkbox-input checkbox-default">
                                        <input id="mens-category" class="styled" type="checkbox" checked>
                                        <label for="mens-category">
                                            Mens  <span class="text-dark">(11)</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-input checkbox-default">
                                        <input id="womens-category" class="styled" type="checkbox" checked>
                                        <label for="womens-category">
                                            Womens <span class="text-dark">(21)</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-input checkbox-default0">
                                        <input id="kids-category" class="styled" type="checkbox" checked>
                                        <label for="kids-category">
                                            Kids <span class="text-dark">(12)</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-input checkbox-default0">
                                        <input id="fashion-category" class="styled" type="checkbox" checked>
                                        <label for="fashion-category">
                                            Fashion <span class="text-dark">(12)</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-input checkbox-default0">
                                        <input id="sportwear-category" class="styled" type="checkbox" checked>
                                        <label for="sportwear-category">
                                            Sportwear <span class="text-dark">(12)</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-input checkbox-default0">
                                        <input id="bags-category" class="styled" type="checkbox" checked>
                                        <label for="bags-category">
                                            Bags <span class="text-dark">(12)</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-input checkbox-default0">
                                        <input id="shoes-category" class="styled" type="checkbox" checked>
                                        <label for="shoes-category">
                                            Shoes <span class="text-dark">(12)</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-input checkbox-default0">
                                        <input id="hoseholds-category" class="styled" type="checkbox" checked>
                                        <label for="hoseholds-category">
                                            HoseHolds <span class="text-dark">(12)</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-input checkbox-default0">
                                        <input id="technology-category" class="styled" type="checkbox" checked>
                                        <label for="technology-category">
                                            Technology <span class="text-dark">(12)</span>
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        </div><!-- end widget -->
                        <div class="widget">
                            <h6 class="subtitle">Prices</h6>
                            
                            <form method="post" class="price-range" data-start-min="250" data-start-max="650" data-min="0" data-max="1000" data-step="1">
                                <div class="ui-range-values">
                                    <div class="ui-range-value-min">
                                        $<span></span>
                                        <input type="hidden">
                                    </div> -
                                    <div class="ui-range-value-max">
                                        $<span></span>
                                        <input type="hidden">
                                    </div>
                                </div>
                                <div class="ui-range-slider"></div>
                                <button type="submit" class="btn btn-default btn-block btn-md">Filter</button>
                            </form>
                        </div><!-- end widget -->
                        <div class="widget">
                            
                            <h6 class="subtitle">Brands</h6>
                            
                            <ul class="list list-unstyled">
                                <li>
                                    <div class="checkbox-input">
                                        <input id="armani-brand" class="styled" type="checkbox" checked>
                                        <label for="armani-brand">
                                            Armani  <span class="text-dark">(11)</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-input">
                                        <input id="gucci-brand" class="styled" type="checkbox" checked>
                                        <label for="gucci-brand">
                                            Gucci <span class="text-dark">(21)</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-input">
                                        <input id="chanel-brand" class="styled" type="checkbox">
                                        <label for="chanel-brand">
                                            Chanel <span class="text-dark">(12)</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-input">
                                        <input id="luis-vuitton-brand" class="styled" type="checkbox">
                                        <label for="luis-vuitton-brand">
                                            Luis Vuitton <span class="text-dark">(12)</span>
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        </div><!-- end widget -->
                        <div class="widget">
                            <h6 class="subtitle">Colors</h6>
                            
                            <ul class="list list-unstyled">
                                <li>
                                    <div class="checkbox-input">
                                        <input id="red-color" class="styled" type="checkbox" checked>
                                        <label for="red-color">
                                            <span class="color" style="background-color: red;"></span> 
                                            Red  
                                            <span class="text-dark">(121)</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-input">
                                        <input id="green-color" class="styled" type="checkbox">
                                        <label for="green-color">
                                            <span class="color" style="background-color: green;"></span>
                                            Green 
                                            <span class="text-dark">(211)</span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox-input">
                                        <input id="blue-color" class="styled" type="checkbox">
                                        <label for="blue-color">
                                            <span class="color" style="background-color: blue;"></span>
                                            Blue 
                                            <span class="text-dark">(120)</span>
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        </div><!-- end widget -->
                        <div class="widget">
                            <h6 class="subtitle">My Cart</h6>
                            
                            <p>There are 2 items in your cart.</p>
                            <hr class="spacer-10">
                            <ul class="items">
                                <li> 
                                    <a href="javascript:void(0);" class="product-image">
                                        <img src="img/products/men_06.jpg" alt="Sample Product ">
                                    </a>
                                    <div class="product-details">
                                        <div class="close-icon"> 
                                            <a href="javascript:void(0);"><i class="fa fa-close"></i></a>
                                        </div>
                                        <p class="product-name"> 
                                            <a href="javascript:void(0);">Lorem ipsum dolor sit amet Consectetur</a> 
                                        </p>
                                        <strong class="text-dark">1</strong> x <span class="price text-primary">$19.99</span>
                                    </div>
                                </li><!-- end item -->
                                <li> 
                                    <a href="javascript:void(0);" class="product-image">
                                        <img src="img/products/shoes_01.jpg" alt="Sample Product ">
                                    </a>
                                    <div class="product-details">
                                        <div class="close-icon"> 
                                            <a href="javascript:void(0);"><i class="fa fa-close"></i></a>
                                        </div>
                                        <p class="product-name"> 
                                            <a href="javascript:void(0);">Lorem ipsum dolor sit amet Consectetur</a> 
                                        </p>
                                        <strong class="text-dark">1</strong> x <span class="price text-primary">$19.99</span>
                                    </div>
                                </li><!-- end item -->
                            </ul>

                            <hr class="spacer-10">
                            <strong class="text-dark">Cart Subtotal:<span class="pull-right text-primary">$19.99</span></strong>
                            <hr class="spacer-10">
                            <a href="checkout.html" class="btn btn-default semi-circle btn-block btn-md"><i class="fa fa-shopping-basket mr-10"></i>Checkout</a>
                        </div><!-- end widget -->
                        
                    </div><!-- end col -->
                    <!-- end sidebar -->
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-12 text-left">
                                <h2 class="title">Shop Full Width Sidebar Left</h2>
                            </div><!-- end col -->
                        </div><!-- end row -->
                        
                        <hr class="spacer-5"><hr class="spacer-20 no-border">
                        
                        <div class="row column-3">
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail store style2">
                                    <div class="header">
                                        <div class="badges">
                                            <span class="product-badge top left primary-background text-white semi-circle">Sale</span>
                                            <span class="product-badge top right text-warning">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i>
                                            </span>
                                        </div>
                                        <figure class="layer">
                                            <a href="javascript:void(0);">
                                                <img class="front" src="img/products/men_01.jpg" alt="">
                                                <img class="back" src="img/products/men_02.jpg" alt="">
                                            </a>
                                        </figure>
                                        <div class="icons">
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                            
                                            <a class="icon" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end col -->
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail store style2">
                                    <div class="header">
                                        <div class="badges">
                                            <span class="product-badge top right danger-background text-white semi-circle">-20%</span>
                                        </div>
                                        <figure class="layer">
                                            <a href="javascript:void(0);">
                                                <img src="img/products/women_01.jpg" alt="">
                                            </a>
                                        </figure>
                                        <div class="icons">
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                            <a class="icon" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end col -->
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail store style2">
                                    <div class="header">
                                        <div class="badges">
                                            <span class="product-badge bottom left warning-background text-white semi-circle">Out of Stock</span>
                                        </div>
                                        <figure class="layer">
                                            <a href="javascript:void(0);">
                                                <img src="img/products/bags_01.jpg" alt="">
                                            </a>
                                        </figure>
                                        <div class="icons">
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end col -->
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail store style2">
                                    <div class="header">
                                        <div class="badges">
                                            <span class="product-badge bottom right info-background text-white semi-circle">New</span>
                                        </div>
                                        <figure class="layer">
                                            <a href="javascript:void(0);">
                                                <img src="img/products/fashion_01.jpg" alt="">
                                            </a>
                                        </figure>
                                        <div class="icons">
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                            <a class="icon" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end col -->
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail store style2">
                                    <div class="header">
                                        <figure class="layer">
                                            <a href="javascript:void(0);">
                                                <img src="img/products/hoseholds_05.jpg" alt="">
                                            </a>
                                        </figure>
                                        <div class="icons">
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                            <a class="icon" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end col -->
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail store style2">
                                    <div class="header">
                                        <figure class="layer">
                                            <a href="javascript:void(0);">
                                                <img src="img/products/kids_01.jpg" alt="">
                                            </a>
                                        </figure>
                                        <div class="icons">
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                            <a class="icon" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end col -->
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail store style2">
                                    <div class="header">
                                        <figure class="layer">
                                            <a href="javascript:void(0);">
                                                <img src="img/products/shoes_01.jpg" alt="">
                                            </a>
                                        </figure>
                                        <div class="icons">
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                            <a class="icon" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end col -->
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail store style2">
                                    <div class="header">
                                        <figure class="layer">
                                            <a href="javascript:void(0);">
                                                <img src="img/products/technology_02.jpg" alt="">
                                            </a>
                                        </figure>
                                        <div class="icons">
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                            <a class="icon" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end col -->
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail store style2">
                                    <div class="header">
                                        <figure class="layer">
                                            <a href="javascript:void(0);">
                                                <img src="img/products/men_04.jpg" alt="">
                                            </a>
                                        </figure>
                                        <div class="icons">
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                            <a class="icon" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end col -->
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail store style2">
                                    <div class="header">
                                        <figure class="layer">
                                            <a href="javascript:void(0);">
                                                <img src="img/products/women_02.jpg" alt="">
                                            </a>
                                        </figure>
                                        <div class="icons">
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                            <a class="icon" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end col -->
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail store style2">
                                    <div class="header">
                                        <figure class="layer">
                                            <a href="javascript:void(0);">
                                                <img src="img/products/bags_05.jpg" alt="">
                                            </a>
                                        </figure>
                                        <div class="icons">
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                            <a class="icon" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end col -->
                            
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail store style2">
                                    <div class="header">
                                        <figure class="layer">
                                            <a href="javascript:void(0);">
                                                <img src="img/products/hoseholds_04.jpg" alt="">
                                            </a>
                                        </figure>
                                        <div class="icons">
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                            <a class="icon" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                            <a class="icon" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h6 class="regular"><a href="shop-single-product-v1.html">Lorem Ipsum dolor sit</a></h6>
                                        <div class="price">
                                            <small class="amount off">$68.99</small>
                                            <span class="amount text-primary">$59.99</span>
                                        </div>
                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                    </div><!-- end caption -->
                                </div><!-- end thumbnail -->
                            </div><!-- end col -->
                        </div><!-- end row -->
                        
                        <hr class="spacer-10 no-border">
                        
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <nav>
                                    <ul class="pagination">
                                        <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li class="active"><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                                    </ul>
                                </nav>
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div><!-- end col -->   
                </div><!-- end row -->
                
                <!-- Modal Product Quick View -->
                <div class="modal fade productQuickView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h5>Lorem ipsum dolar sit amet</h5>
                            </div><!-- end modal-header -->
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class='carousel slide product-slider' data-ride='carousel' data-interval="false">
                                            <div class='carousel-inner'>
                                                <div class='item active'>
                                                    <figure>
                                                        <img src='img/products/men_01.jpg' alt='' />
                                                    </figure>
                                                </div><!-- end item -->
                                                <div class='item'>
                                                    <div class="embed-responsive embed-responsive-16by9">
                                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/NrmMk1Myrxc"></iframe>
                                                    </div>
                                                </div><!-- end item -->
                                                <div class='item'>
                                                    <figure>
                                                        <img src='img/products/men_03.jpg' alt='' />
                                                    </figure>
                                                </div><!-- end item -->
                                                <div class='item'>
                                                    <figure>
                                                        <img src='img/products/men_04.jpg' alt='' />
                                                    </figure>
                                                </div><!-- end item -->
                                                <div class='item'>
                                                    <figure>
                                                        <img src='img/products/men_05.jpg' alt=''/>
                                                    </figure>
                                                </div><!-- end item -->

                                                <!-- Arrows -->
                                                <a class='left carousel-control' href='.html' data-slide='prev'>
                                                    <span class='fa fa-angle-left'></span>
                                                </a>
                                                <a class='right carousel-control' href='.html' data-slide='next'>
                                                    <span class='fa fa-angle-right'></span>
                                                </a>
                                            </div><!-- end carousel-inner -->

                                            <!-- thumbs -->
                                            <ol class='carousel-indicators mCustomScrollbar meartlab'>
                                                <li data-target='.product-slider' data-slide-to='0' class='active'><img src='img/products/men_01.jpg' alt='' /></li>
                                                <li data-target='.product-slider' data-slide-to='1'><img src='img/products/men_02.jpg' alt='' /></li>
                                                <li data-target='.product-slider' data-slide-to='2'><img src='img/products/men_03.jpg' alt='' /></li>
                                                <li data-target='.product-slider' data-slide-to='3'><img src='img/products/men_04.jpg' alt='' /></li>
                                                <li data-target='.product-slider' data-slide-to='4'><img src='img/products/men_05.jpg' alt='' /></li>
                                                <li data-target='.product-slider' data-slide-to='5'><img src='img/products/men_06.jpg' alt='' /></li>
                                            </ol><!-- end carousel-indicators -->
                                        </div><!-- end carousel -->
                                    </div><!-- end col -->
                                    <div class="col-sm-7">
                                        <p class="text-gray alt-font">Product code: 1032446</p>

                                        <i class="fa fa-star text-warning"></i>
                                        <i class="fa fa-star text-warning"></i>
                                        <i class="fa fa-star text-warning"></i>
                                        <i class="fa fa-star text-warning"></i>
                                        <i class="fa fa-star-half-o text-warning"></i>
                                        <span>(12 reviews)</span>
                                        <h4 class="text-primary">$79.00</h4>
                                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                        <hr class="spacer-10">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <select class="form-control" name="select">
                                                    <option value="" selected>Color</option>
                                                    <option value="red">Red</option>
                                                    <option value="green">Green</option>
                                                    <option value="blue">Blue</option>
                                                </select>
                                            </div><!-- end col -->
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <select class="form-control" name="select">
                                                    <option value="">Size</option>
                                                    <option value="">S</option>
                                                    <option value="">M</option>
                                                    <option value="">L</option>
                                                    <option value="">XL</option>
                                                    <option value="">XXL</option>
                                                </select>
                                            </div><!-- end col -->
                                            <div class="col-md-4 col-sm-12">
                                                <select class="form-control" name="select">
                                                    <option value="" selected>QTY</option>
                                                    <option value="">1</option>
                                                    <option value="">2</option>
                                                    <option value="">3</option>
                                                    <option value="">4</option>
                                                    <option value="">5</option>
                                                    <option value="">6</option>
                                                    <option value="">7</option>
                                                </select>
                                            </div><!-- end col -->
                                        </div><!-- end row -->
                                        <hr class="spacer-10">
                                        <ul class="list list-inline">
                                            <li><button type="button" class="btn btn-default btn-md round"><i class="fa fa-shopping-basket mr-5"></i>Add to Cart</button></li>
                                            <li><button type="button" class="btn btn-gray btn-md round"><i class="fa fa-heart mr-5"></i>Add to Wishlist</button></li>
                                        </ul>
                                    </div><!-- end col -->
                                </div><!-- end row -->
                            </div><!-- end modal-body -->
                        </div><!-- end modal-content -->
                    </div><!-- end modal-dialog -->
                </div><!-- end productRewiew -->
                
            </div><!-- end container -->
        </section>
        <!-- end section -->
        
<?php 

    include "footer.php";

?>