<?php

include 'header.php';

?>

 <!-- start section -->
        <section class="section white-backgorund">
            <div class="container">
                <div class="row">
                    
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12 text-left">
                                <h2 class="title" style="margin-top: 0px;">My Account</h2>
                            </div><!-- end col -->
                        </div><!-- end row -->
                        <hr class="spacer-5"><hr class="spacer-20 no-border">
                        <div class="row" >
                            <div class="col-sm-3 text-left" >
                                <ul style="list-style-type: none;padding-left: 0px;">
                                    <li><a href="">Profile</a></li>
                                    <li><a href="">Upgrade Account</a></li>
                                    <li><a href="">Update Document</a></li>
                                    <li><a href="">Post Products/Services</a></li>
                                    <li><a href=""><strong>Workman/Equipment Details</strong></a></li>
                                </ul>
                            </div>
                            <div class="col-sm-9 text-left" style="border-left: 1px solid rgba(0, 0, 0, 0.1);" >
                                <h5 class="title">Post Workman / Equipment Details</h5>
                                <br>
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#workTab" aria-controls="workTab" role="workTab" data-toggle="tab">Workman Details</a></li>
                                    <li role="presentation"><a href="#equipmentTab" aria-controls="equipment" role="equipment" data-toggle="tab">Equipment Details</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="workTab">
                                    <!-- start of Workman -->
                                    <div class="table-responsive">    
                                      <div class="row">
                                        <div class="col-md-3">
                                           <label for ="firstname">Workman name</label>
                                           <input id="workman_name" type="text" placeholder="Full Name" name="firstname" class="form-control input-sm required">
                                        </div>
                                        <div class="col-md-3 workman-pro">
                                            <p class="workman-photo-head">WorkMan Images</p>
                                            <figure class="user-avatar medium">
                                                    <img class='weImg' alt="avatar" src='img/avatar.png' style='height:65px;width:65px' >
                                                    <br>
                                                    <button id="workman-photo" class="work-up-btn" onclick="document.getElementById('work-man-photo').click(); return false;">Upload</button>
                                                    <input type='file' id="work-man-photo" onchange="readURL(this);" style="visibility: hidden;">
                                            </figure>
                                        </div>
                                        <div class="col-md-3 workman-pro">
                                            <p class="workman-aadhar-head">Aadhar</p>
                                            <figure class="user-avatar medium">
                                                    <img class='weImg' alt="avatar" src='img/avatar.png' style='height:65px;width:65px' >
                                                    <br>
                                                    <button id="workman-aadhar" class="work-up-btn" onclick="document.getElementById('work-man-aadhar').click(); return false;">Upload</button>
                                                    <input type='file' id="work-man-aadhar" onchange="readURL(this);" style="visibility: hidden;">
                                            </figure>
                                        </div>  
                                       <div class="col-md-3 workman-pro">
                                        <p class="workman-dox-hea">Address Proof</p>
                                            <figure class="user-avatar medium">
                                                    <img class='weImg' alt="avatar" src='img/avatar.png' style='height:65px;width:65px' >
                                                    <br>
                                                    <button id="workman-addproof" class="work-up-btn" onclick="document.getElementById('work-man-addproof').click(); return false;">Upload</button>
                                                    <input type='file' id="work-man-addproof" onchange="readURL(this);" style="visibility: hidden;">
                                            </figure>
                                            
                                        </div>  
                                        <a id='addWorkMan'  class="btn btn-default round btn-sm"><i class="fa fa-plus mr-5"></i> Add Workman</a>
                                      </div>  

                                    <!-- start of workman list -->
                                    <hr>
                                        <div class=''>
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Workman image</th>
                                                    <th>Workman Name</th>
                                                    <th>Aadhar</th>
                                                    <th>License</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><img src='img/avatar.png' alt="" style='height:65px;width:65px'></td>
                                                    <td>XXXXXXX</td>
                                                    <td><img src='img/avatar.png' alt="" style='height:65px;width:65px'></td>
                                                    <td><img src='img/avatar.png' alt="" style='height:65px;width:65px'></td>
                                                    <td>
                                                    <input type='hidden' id='#' value='#'>
                                                    <input type='hidden' id='#' value='#'>
                                                    <input type='hidden' id='#' value='#'>
                                                    <a href="javascript:void(0);" class="btn btn-warning round btn-xs edit_workman" id='{{$dat->id}}' data-toggle="modal" data-target="#myModal">Edit</a>
                                                    <a href="deleteWorkMan/{{$dat->id}}" class="btn btn-danger round btn-xs">Delete</a>
                                                    </td>
                                                </tr>
                <!-- @endforeach -->
                                                </tbody>
                                            </table>
                                        </div>
                                    <!-- end of workman list -->

                                        </div>
                                    <!-- end of workman -->
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="equipmentTab">
                                            <div class="table-responsive">    
                                                <div class="row">
                                        <div class="col-md-3">
                                           <label for ="firstname">Equipment name</label>
                                           <input id="equipment_name" type="text" placeholder="Full Name" name="firstname" class="form-control input-sm required">
                                        </div>
                                        <div class="col-md-3 workman-pro">
                                            <p class="equipment-photo-head">Equipment Image</p>
                                            <figure class="equipment-avatar medium">
                                                    <img class='weImg' alt="avatar" src='img/avatar.png' style='height:65px;width:65px' >
                                                    <br>
                                                    <button id="equipment-photo" class="work-up-btn" onclick="document.getElementById('work-man-photo').click(); return false;">Upload</button>
                                                    <input type='file' id="work-man-photo" onchange="readURL(this);" style="visibility: hidden;">
                                            </figure>
                                        </div>
                                        <div class="col-md-3 workman-pro">
                                            <p class="workman-aadhar-head">RC Book</p>
                                            <figure class="user-avatar medium">
                                                    <img class='weImg' alt="avatar" src='img/avatar.png' style='height:65px;width:65px' >
                                                    <br>
                                                    <button id="workman-aadhar" class="work-up-btn" onclick="document.getElementById('work-man-aadhar').click(); return false;">Upload</button>
                                                    <input type='file' id="work-man-aadhar" onchange="readURL(this);" style="visibility: hidden;">
                                            </figure>
                                        </div>  
                                       <div class="col-md-3 workman-pro">
                                        <p class="workman-dox-hea">Permit Image</p>
                                            <figure class="user-avatar medium">
                                                    <img class='weImg' alt="avatar" src='img/avatar.png' style='height:65px;width:65px' >
                                                    <br>
                                                    <button id="workman-addproof" class="work-up-btn" onclick="document.getElementById('work-man-addproof').click(); return false;">Upload</button>
                                                    <input type='file' id="work-man-addproof" onchange="readURL(this);" style="visibility: hidden;">
                                            </figure>
                                            
                                        </div>  
                                        <a id='addWorkMan'  class="btn btn-default round btn-sm"><i class="fa fa-plus mr-5"></i> Add Equipment</a>
                                      </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>         

<?php

include 'footer.php';

?>