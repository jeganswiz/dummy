<!DOCTYPE html>
<html lang="en">

<head>
    <title>7Atara</title>
    <meta charset="utf-8">
    <meta name="description" content="7ATARA E-Commerce">
    <meta name="author" content="Mello" />
    <meta name="keywords" content="plus, html5, css3, template, ecommerce, e-commerce, bootstrap, responsive, creative" />        
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <!--jquery cdn link-->

    <!-- state dropdown link -->
    <link href="https://unpkg.com/multiple-select@1.3.0/dist/multiple-select.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://unpkg.com/multiple-select@1.3.0/dist/multiple-select.js"></script>          

    
    
    
    <link href="https://fonts.googleapis.com/css?family=Lato|Roboto" rel="stylesheet">
    <!--Favicon-->
    <link rel="shortcut icon" href="img/7a-log.ico" type="image/x-icon">
    <link rel="icon" href="img/7a-log.ico" type="image/x-icon">    
    <!-- css files -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css" />
    <link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css" />
    <link rel="stylesheet" type="text/css" href="css/animate.css" />
    <link rel="stylesheet" type="text/css" href="css/swiper.css" />

    <!-- this is default skin you can replace that with: dark.css, yellow.css, red.css ect -->
    <link id="pagestyle" rel="stylesheet" type="text/css" href="css/default.css" />
    <link rel="stylesheet" type="text/css" href="css/override.css"s>
    <!-- Google fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700i" rel="stylesheet"> -->

</head>
    <body onscroll="hide()">

        <!-- start section -->
        <div class="primary-background hidden-xs">
            <div class="container-fluid">
                <div class="row grid-space-0">
                    <div class="col-sm-12">
                        <figure>
                            <a href="categories.php">
                                <img src="img/banners/top_banner.jpg" alt=""/>
                            </a>
                        </figure>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div>
        <!-- end section -->

        <!-- start topBar -->


        <div class="middleBar">
            <div class="container">
                <div class="row display-table">
                    <div class="col-sm-2 vertical-align text-left hidden-md hidden-xs">
                        <a href="index.php">    
                            <img width="160px" height="80px" src="img/logo.png" alt="" />
                        </a>
                    </div><!-- end col -->
                    <div class="col-sm-8 vertical-align text-center search-width">
                        <form>
                            <div class="row grid-space-1 oneline-form" style="margin-right: 180px;">
                              <div class="col-sm-3">
                                  <select class="form-control input-lg category-box" name="category" style="border-bottom-left-radius:2px;border-top-left-radius:2px;">
                                      <option value="all">Goods</option>
                                      <option value="all">Services</option>
                                      
                                  </select>
                              </div>
                                <div class="col-sm-3">
                                    <input type="text" name="keyword" class="form-control input-lg" placeholder="Search" style="width: 200px;">
                                </div><!-- end col -->
                                <!-- end col -->
                                <div class="col-sm-2">
                                    <button class="btn btn-default btn-block btn-lg" style="height: 40px;width: 50px;border-bottom-right-radius:2px;border-top-right-radius:2px;margin-left: px;"><i class="fa fa-search" style="font-size: 24px; margin-top:-8px;"></i></button>
                                </div><!-- end col -->
                                <div class="location-content col-sm-4">
                                    <div class="getlocation-navbar">
                                    <a href="#" onclick="getLocation();" class="mapicon-cur"><i class="fa fa-map-marker location-ico"></i></a>
                                    <span id="getlocation_addr"></span>
                                    </div>

                                </div>

                            </div><!-- end row -->
                            
                            

                        </form>
                    </div><!-- end col -->
                    <div class="topBar">
                      <ul class="topBarNav pull-right">
                        <li class="linkdown">
                            <a href="javascript:void(0);">
                                <i class="fa fa-user fa-2 mr-5"></i>
                                <span class="hidden-xs">

                                    <i class="fa fa-angle-down fa-2 ml-5"></i>
                                </span>
                            </a>
                            <ul class="w-150">
                                <li><a href="login_register.php">Login</a></li>
                                <li><a href="login_register.php">Create Account</a></li>
                                <li><a href="profille.php">My Profile</a></li>
                                <li class="divider"></li>
                                <li><a href="wishlist.php">Wishlist (5)</a></li>
                                <li><a href="cart.php">My Cart</a></li>
                                <li><a href="checkout.php">Checkout</a></li>
                                <li><a href="#" id='logout'name='logout'>Logout</a></li>
                            </ul>
                        </li>
                        <li class="linkdown pull-right">
                            <a href="javascript:void(0);">
                                <i class="fa fa-shopping-basket fa-2 mr-5"></i>
                                <sup class="text-primary2">(3)</sup>
                                <span class="hidden-xs">
                                    <i class="fa fa-angle-down fa-2 ml-5"></i>
                                </span>
                            </a>
                            <ul class="cart w-250">
                                <li>
                                    <div class="cart-items">
                                        <ol class="items">
                                            <li>
                                                <a href="shop-single.php" class="product-image">
                                                    <img src="img/products/men_06.jpg" alt="Sample Product ">
                                                </a>
                                                <div class="product-details">
                                                    <div class="close-icon">
                                                        <a href="javascript:void(0);"><i class="fa fa-close"></i></a>
                                                    </div>
                                                    <p class="product-name">
                                                        <a href="shop-single.php">Lorem Ipsum dolor sit</a>
                                                    </p>
                                                    <strong>1</strong> x <span class="price text-primary">$59.99</span>
                                                </div><!-- end product-details -->
                                            </li><!-- end item -->
                                            <li>
                                                <a href="shop-single.php" class="product-image">
                                                    <img src="img/products/shoes_01.jpg" alt="Sample Product ">
                                                </a>
                                                <div class="product-details">
                                                    <div class="close-icon">
                                                        <a href="javascript:void(0);"><i class="fa fa-close"></i></a>
                                                    </div>
                                                    <p class="product-name">
                                                        <a href="shop-single.php">Lorem Ipsum dolor sit</a>
                                                    </p>
                                                    <strong>1</strong> x <span class="price text-primary">$39.99</span>
                                                </div><!-- end product-details -->
                                            </li><!-- end item -->
                                            <li>
                                                <a href="shop-single.php" class="product-image">
                                                    <img src="img/products/bags_07.jpg" alt="Sample Product ">
                                                </a>
                                                <div class="product-details">
                                                    <div class="close-icon">
                                                        <a href="javascript:void(0);"><i class="fa fa-close"></i></a>
                                                    </div>
                                                    <p class="product-name">
                                                        <a href="shop-single.php">Lorem Ipsum dolor sit</a>
                                                    </p>
                                                    <strong>1</strong> x <span class="price text-primary">$29.99</span>
                                                </div><!-- end product-details -->
                                            </li><!-- end item -->
                                        </ol>
                                    </div>
                                </li>
                                <li>
                                    <div class="cart-footer">
                                        <a href="order-list.php" class="pull-left"><i class="fa fa-cart-plus mr-5"></i>View Cart</a>
                                        <a href="checkout.php" class="pull-right"><i class="fa fa-shopping-basket mr-5"></i>Checkout</a>
                                    </div>
                                </li>
                            </ul>
                          </li>
                        </ul>
                      </div>
                    </div><!-- end col -->
                </div><!-- end  row -->
            </div><!-- end container --><!-- end middleBar -->

        <!-- start navbar -->
        <div class="navbar yamm navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#navbar-collapse-3" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="javascript:void(0);" class="navbar-brand visible-xs">
                        <img width = "120px" src="img/logo.png" alt="logo">
                    </a>
                </div>
                <div id="navbar-collapse-3" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <!-- Home -->
                        <li class="dropdown active"><a href="#" data-toggle="dropdown" class="dropdown-toggle nav-heading">Best Deals<i class="fa fa-angle-down ml-5"></i></a>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="home-v1.html">Home - Version 1</a></li>
                                <li><a href="home-v2.html">Home - Version 2</a></li>
                                <li><a href="home-v3.html">Home - Version 3</a></li>
                                <li><a href="home-v4.html">Home - Version 4 <span class="label primary-background">1.1</span></a></li>
                                <li class="active"><a href="home-v5.html">Home - Version 5 <span class="label primary-background">1.1</span></a></li>
                                <li><a href="home-v6.html">Home - Version 6 <span class="label primary-background">1.2</span></a></li>
                                <li><a href="home-v7.html">Home - Version 7 <span class="label primary-background">1.3</span></a></li>
                            </ul><!-- end ul dropdown-menu -->
                        </li><!-- end li dropdown -->
                        <!-- Features -->
                        <li class="dropdown left"><a href="#" data-toggle="dropdown" class="dropdown-toggle">New Deals<i class="fa fa-angle-down ml-5"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="headers.html">Headers</a></li>
                                <li><a href="footers.html">Footers</a></li>
                                <li><a href="sliders.html">Sliders</a></li>
                                <li><a href="typography.html">Typography</a></li>
                                <li><a href="grid.html">Grid</a></li>
                                <li class="divider"></li>
                                <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Dropdown Level 1</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Dropdown Level</a></li>
                                        <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Dropdown Level 2</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0);">Dropdown Level</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul><!-- end ul dropdown-menu -->
                        </li><!-- end li dropdown -->
                        <!-- Pages -->

                        <!-- elements -->

                        <!-- Collections -->
                        <li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Made in India<i class="fa fa-angle-down ml-5"></i></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="yamm-content">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3">
                                                <a href="javascript:void(0);">
                                                    <figure class="zoom-out">
                                                        <img alt="" src="img/banners/collection_01.jpg">
                                                    </figure>
                                                </a>
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <a href="javascript:void(0);">
                                                    <figure class="zoom-in">
                                                        <img alt="" src="img/banners/collection_02.jpg">
                                                    </figure>
                                                </a>
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <a href="javascript:void(0);">
                                                    <figure class="zoom-out">
                                                        <img alt="" src="img/banners/collection_03.JPG">
                                                    </figure>
                                                </a>
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <a href="javascript:void(0);">
                                                    <figure class="zoom-in">
                                                        <img alt="" src="img/banners/collection_04.JPG">
                                                    </figure>
                                                </a>
                                            </div><!-- end col -->
                                        </div><!-- end row -->

                                        <hr class="spacer-20 no-border">

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3">
                                                <h6>Pellentes que nec diam lectus</h6>
                                                <p>Proin pulvinar libero quis auctor pharet ra. Aenean fermentum met us orci, sedf eugiat augue pulvina r vitae. Nulla dolor nisl, molestie nec aliquam vitae, gravida sodals dolor...</p>
                                                <button type="button" class="btn btn-default round btn-md">Read more</button>
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="thumbnail store style1">
                                                    <div class="header">
                                                        <div class="badges">
                                                            <span class="product-badge top left white-backgorund text-primary semi-circle">Sale</span>
                                                            <span class="product-badge top right text-primary">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half-o"></i>
                                                            </span>
                                                        </div>
                                                        <figure class="layer">
                                                            <img src="img/products/men_01.jpg" alt="">
                                                        </figure>
                                                        <div class="icons">
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="caption">
                                                        <h6 class="thin"><a href="javascript:void(0);">Lorem Ipsum dolor sit</a></h6>
                                                        <div class="price">
                                                            <small class="amount off">$68.99</small>
                                                            <span class="amount text-primary">$59.99</span>
                                                        </div>
                                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                                    </div><!-- end caption -->
                                                </div><!-- end thumbnail -->
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="thumbnail store style1">
                                                    <div class="header">
                                                        <div class="badges">
                                                            <span class="product-badge top left white-backgorund text-primary semi-circle">Sale</span>
                                                            <span class="product-badge top right text-primary">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half-o"></i>
                                                            </span>
                                                        </div>
                                                        <figure class="layer">
                                                            <img src="img/products/women_01.jpg" alt="">
                                                        </figure>
                                                        <div class="icons">
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="caption">
                                                        <h6 class="thin"><a href="javascript:void(0);">Lorem Ipsum dolor sit</a></h6>
                                                        <div class="price">
                                                            <small class="amount off">$68.99</small>
                                                            <span class="amount text-primary">$59.99</span>
                                                        </div>
                                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                                    </div><!-- end caption -->
                                                </div><!-- end thumbnail -->
                                            </div><!-- end col -->
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="thumbnail store style1">
                                                    <div class="header">
                                                        <div class="badges">
                                                            <span class="product-badge top left white-backgorund text-primary semi-circle">Sale</span>
                                                            <span class="product-badge top right text-primary">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-half-o"></i>
                                                            </span>
                                                        </div>
                                                        <figure class="layer">
                                                            <img src="img/products/kids_01.jpg" alt="">
                                                        </figure>
                                                        <div class="icons">
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-heart-o"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);"><i class="fa fa-gift"></i></a>
                                                            <a class="icon semi-circle" href="javascript:void(0);" data-toggle="modal" data-target=".productQuickView"><i class="fa fa-search"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="caption">
                                                        <h6 class="thin"><a href="javascript:void(0);">Lorem Ipsum dolor sit</a></h6>
                                                        <div class="price">
                                                            <small class="amount off">$68.99</small>
                                                            <span class="amount text-primary">$59.99</span>
                                                        </div>
                                                        <a href="javascript:void(0);"><i class="fa fa-cart-plus mr-5"></i>Add to cart</a>
                                                    </div><!-- end caption -->
                                                </div><!-- end thumbnail -->
                                            </div><!-- end col -->
                                        </div><!-- end row -->
                                    </div><!-- end yamm-content -->
                                </li><!-- end li -->
                            </ul><!-- end dropdown-menu -->
                        </li><!-- end dropdown -->
                        <li class="dropdown left"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Near You<i class="fa fa-angle-down ml-5"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="headers.html">Headers</a></li>
                                <li><a href="footers.html">Footers</a></li>
                                <li><a href="sliders.html">Sliders</a></li>
                                <li><a href="typography.html">Typography</a></li>
                                <li><a href="grid.html">Grid</a></li>
                                <li class="divider"></li>
                                <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Dropdown Level 1</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Dropdown Level</a></li>
                                        <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Dropdown Level 2</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0);">Dropdown Level</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul><!-- end ul dropdown-menu -->
                        </li><!-- end li dropdown -->
                        <li class="dropdown left"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Popular categories<i class="fa fa-angle-down ml-5"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="headers.html">Headers</a></li>
                                <li><a href="footers.html">Footers</a></li>
                                <li><a href="sliders.html">Sliders</a></li>
                                <li><a href="typography.html">Typography</a></li>
                                <li><a href="grid.html">Grid</a></li>
                                <li class="divider"></li>
                                <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Dropdown Level 1</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Dropdown Level</a></li>
                                        <li class="dropdown-submenu"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Dropdown Level 2</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0);">Dropdown Level</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul><!-- end ul dropdown-menu -->
                        </li><!-- end li dropdown -->

                    </ul><!-- end navbar-nav -->
                    <ul class="nav navbar-nav navbar-right">
                      <li class="dropdown right "><a href="refer.php" id="unique">Market & Earn</a></li>
                      <li class="dropdown right "><a href="updatedox.php" id="unique">Sell Products</a></li>
                      <li class="dropdown right "><a href="post.php" id="unique">Post Services</a></li>
                    </ul>
                  <!-- end navbar-right -->
                </div><!-- end navbar collapse -->
            </div><!-- end container -->
        </div><!-- end navbar -->


<script>
var x = document.getElementById("getlocation_addr");

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else { 
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
}

function showPosition(position) {
  // x.innerHTML = "Latitude: " + position.coords.latitude + 
  // "<br>Longitude: " + position.coords.longitude;

  var locAPI= "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDtEXD8bpudLluBK9a_WVflZ7IOAwkqmUY&latlng="+position.coords.latitude+","+position.coords.longitude+"&sensor=true";

  // x.innerHTML= locAPI;
  $.get({
    url : locAPI,
    success : function(data){

        console.log(data);
        
        x.innerHTML += data.results[0].address_components[3].long_name+" ,";
        x.innerHTML += data.results[0].address_components[5].long_name+" .";

    }
  });



  }
</script>
