<html>
    <head>
        <title>multi select</title>
        <link rel="stylesheet" href="mselect/chosen.min.css">
        <script src="js/jquery-3.4.1.min.js"></script>
        <script src="mselect/chosen.jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>     

    </head>
    <body>
<div style="margin-top:100px;padding:200px;">
    <div class="col-md-4">
            <select id="xxx" multiple="" style="width:400px;">
                <option>one</option>
                <option>two</option>
                <option>five</option>
                <option>four</option>
                <option>thre</option>
            </select>
    </div>
    <div class="col-md-4">
            <select id="yyy" multiple="" style="width:400px;">
                <option>one</option>
                <option>two</option>
                <option>five</option>
                <option>four</option>
                <option>thre</option>
            </select>
    </div>
    <div class="col-md-4">
            <select id="zzz" multiple="" style="width:400px;">
                <option>one</option>
                <option>two</option>
                <option>five</option>
                <option>four</option>
                <option>thre</option>
            </select>
    </div>
</div>

        <script>
            $(document).ready(function(){
                $("#xxx").chosen();
                $("#yyy").chosen();
                $("#zzz").chosen();
            });
        </script>
    </body>
    </html>