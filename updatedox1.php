<?php
include 'header.php'
?>

<script>
    

    function applyImage(inputField, target, errorTarget) {

        $(errorTarget).html("");

        var input = inputField;
        var url = $(inputField).val();
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(target).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            $(errorTarget).html("invalid file");
        }

    }

    $(function() {
        $("#pro_pan_img").change(function() {
            applyImage(this, "#imgPan", "#pan_err");
        });
        $("#pro_aadhar_img").change(function() {
            applyImage(this, "#imgAadhar", "#aadhar_err");
        });
        $("#pro_digi_img").change(function() {
            applyImage(this, "#imgSign", "#digi_err");
        })
    })
</script>
<!-- start section -->
<div class="section white-backgorund">
    <div class="container">
        <div class="row">

            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12 text-left">
                        <h2 class="title" style="margin-top: 10px;">My Account</h2>
                    </div><!-- end col -->
                </div><!-- end row -->
                <hr class="spacer-5">
                <hr class="spacer-20 no-border">
                <div class="row">
                    <div class="col-sm-2 text-left">
                        <ul style="list-style-type: none;padding-left: 0px;">
                            <li><a href="profille.php">Profile</a></li>
                            <li><a href="updateaccount.php">Upgrade Account</a></li>
                            <li><a href="updatedoxservice.php"><strong>Update Document Serice</strong></a></li>
                            <li><a href="updatedox.php"><strong>Update Document Product</strong></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-10 text-left" style="border-left: 1px solid rgba(0, 0, 0, 0.1);">
                        <h4 style="margin-top: 0px;">Update Document</h4>
                        <div class="row" style="text-align: center;">
                            <div class="col-sm-3" style="border: 1px solid rgba(0, 0, 0, 0.1);height: 200px;margin: 10px 10px;padding: 0px;">
                                <h5 style="background-color: rgba(0, 0, 0, 0.1); margin: 0px;padding: 10px;">Bussiness Details</h5>
                                <p>You need to provide your GSTIN , TAN and other bussiness information</p>
                                <a class="btn btn-default round btn-md" data-toggle="modal" data-target="#bussinessPopup">Add Details</a>
                            </div>
                            <div class="col-sm-3" style="border: 1px solid rgba(0, 0, 0, 0.1);height: 200px;margin: 10px 10px;padding: 0px;">
                                <h5 style="background-color: rgba(0, 0, 0, 0.1);margin: 0px; padding: 10px;">Bank Details</h5>
                                <p>we need your bank account details and KYC documents to verify your bank account</p>
                                <a class="btn btn-default round btn-md" data-toggle="modal" data-target="#bankPopup">Add Details</a>
                            </div>
                            <div class="col-sm-4" style="border: 1px solid rgba(0, 0, 0, 0.1);height: 200px;margin: 10px 10px;padding: 0px;">
                                <h5 style="background-color: rgba(0, 0, 0, 0.1);margin: 0px; padding: 10px;">Post Product</h5>
                                <p>we need your bank account details and KYC documents to verify your bank account</p>
                                <a class="btn btn-default round btn-md" href="post.php">Add Details</a>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal starts here -->
<div class="modal fade" id="bussinessPopup" tabindex="-1" role="dialog" aria-labelledby="bussinessDetails" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="bussinessDetails">Business Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="businessForm" method="get" action="">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="b_company">Company Name</label>
                            <input type="text" class="form-control" id="b_company" placeholder="Company Name" name="b_company">
                        </div>
                        <div class="form-group">
                            <label for="b_type">Business Type</label>
                            <input type="radio" name="b_type" id="b_type" value="option1"> Wholesaler
                            <input type="radio" name="b_type" id="b_type" value="option2"> Retailer
                            <input type="radio" name="b_type" id="b_type" value="option3"> Distributor
                            <input type="radio" name="b_type" id="b_type" value="option4"> Manufacturer
                            <input type="radio" name="b_type" id="b_type" value="option5"> Others
                        </div>
                        <div class="form-group">
                            <label for="b_gstin">GSTIN Number</label>
                            <input type="text" class="form-control" id="b_gstin" placeholder="GSTIN Number" name="b_gstin">
                        </div>
                        <div class="form-group">
                            <label for="b_address">Company Address</label>
                            <input class="form-control" id="b_address" placeholder="Company Address" name="b_address">
                            <input type='hidden' id='b_lat'>
                            <input type='hidden' id='b_lng'>
                        </div>
                        <div class="form-group">
                            <label for="bill_address">Billing Address</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <input class="form-control bill-add" id="b_billaddress" placeholder="Billing Address" name="c_address">
                                </div>
                                <div class="col-md-6">
                                    <input type="checkbox" id="sameas"> Click here if same as company's Address
                                </div>
                            </div>
                            <input type='hidden' id='b_lat'>
                            <input type='hidden' id='b_lng'>
                        </div>
                        <div class="form-group">
                            <label for="b_pan_no">PAN Number</label>
                            <input type="text" class="form-control" id="b_pan_no" placeholder="PAN Number" name="b_pan_no">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">PAN Card Image</label>

                            <figure class="user-avatar medium">
                                <img id="imgPan" class='panImg' alt="avatar" src='img/avatar.png' style='height:65px;width:65px'>
                                <button id="equipment-photo" class="work-up-btn" onclick="document.getElementById('pro_pan_img').click(); return false;">Upload</button>
                                <input type='file' id="pro_pan_img" style="visibility: hidden;">
                                <p id="pan_err"></p>
                                <span id='pan_status'></span>
                            </figure>
                        </div>
                        <div class="form-group">
                            <label for="b_tan_no">TAN Number</label>
                            <input type="text" class="form-control" id="b_tan_no" placeholder="TAN Number" name="b_tan_no">
                        </div>
                        <div class="form-group">
                            <label for="b_aadhar">Aadhar Number</label>
                            <input type="text" class="form-control" placeholder="Aadhar Number" name="b_aadhar">
                        </div>
                        <div class="row service-proof">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Aadhar Image</label>

                                    <figure class="user-avatar medium">
                                        <img id="imgAadhar" class='aadharImg' alt="avatar" src='img/avatar.png' style='height:65px;width:65px'>
                                        <button id="equipment-photo" class="work-up-btn" onclick="document.getElementById('pro_aadhar_img').click(); return false;">Upload</button>
                                        <input type='file' id="pro_aadhar_img" style="visibility: hidden;">
                                        <p id="aadhar_err"></p>
                                        <span id='aadhar_status'></span>
                                    </figure>
                                    <span class="error" id="aadharUrlHTML"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Digital Image Signature</label>

                                    <figure class="user-avatar medium">
                                        <img id="imgSign" class='digImg' alt="avatar" src='img/avatar.png' style='height:65px;width:65px'>
                                        <button id="equipment-photo" class="work-up-btn" onclick="document.getElementById('pro_digi_img').click(); return false;">Upload</button>
                                        <input type='file' id="pro_digi_img" onchange="readURL(this);" style="visibility: hidden;">
                                        <p id="digi_err"></p>
                                        <span id='dig_status'></span>
                                    </figure>
                                    <span class="error" id="digUrlHTML"></span>
                                    <input type="hidden" id="aadharUrl" value="{{Auth::user()->aadhar_image}}">
                                    <input type="hidden" id="panUrl" value="{{Auth::user()->pan_image}}">
                                    <input type="hidden" id="digUrl" value="{{Auth::user()->dig_image}}">
                                    <input type="hidden" id="ppanUrl" value="{{Auth::user()->p_pan_image}}">
                                    <input type="hidden" id="addrUrl" value="{{Auth::user()->address_img}}">
                                    <input type="hidden" id="ccUrl" value="{{Auth::user()->cheque_img}}">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <input type="button" class="btn btn-primary" name="submit" id="pro_busi_btn" value="Save Changes">
                            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="bankPopup" tabindex="-1" role="dialog" aria-labelledby="bankDetails" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="bankDetails">Bank Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="bankForm" method="get" action="">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="ac_name">Account Holder Name</label>
                            <input type="text" class="form-control" id="pro_ac_name" placeholder="Account Holder Name" name="pro_ac_name">
                        </div>
                        <div class="form-group">
                            <label for="ac_no">Account Number</label>
                            <input type="text" class="form-control" id="pro_ac_no" placeholder="Account Number" name="pro_ac_no">
                        </div>
                        <div class="form-group ">
                            <label for="ac_no2">Confirm Account Number</label>
                            <input type="text" class="form-control" id="pro_ac_no2" placeholder="Confirm Account Number" name="pro_ac_no2">
                        </div>
                        <div class="form-group">
                            <label for="ac_b_name">Bank Name</label>
                            <input type="text" class="form-control" id="pro_ac_b_name" placeholder="Bank Name" name="pro_ac_b_name">
                        </div>
                        <div class="form-group">
                            <label for="ac_ifsc">IFSC Number</label>
                            <a class='pull-right' href='https://bankifsccode.com/' target="_blank">Click here to find your IFSC Code</a>
                            <input type="text" class="form-control" id="pro_ac_ifsc" placeholder="IFSC Number" name="pro_ac_ifsc">
                        </div>
                        <div class="form-group">
                            <h5>KYC Documents</h5>
                            <label for="pro_ac_btype">Business Type</label>
                            <select class='form-control' id='pro_btype' name="ac_btype">
                                <option value=''>Choose any option</option>
                                <option value='0'>Wholesaler</option>
                                <option value='1'>Retailer</option>
                                <option value='2'>Manufacturer</option>
                                <option value='3'>Distributor</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="ac_p_pan">Personal PAN Card Number</label>
                            <input type="text" class="form-control" id="pro_ac_p_pan" placeholder="Personal PAN Number" name="pro_ac_p_pan">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Personal PAN Card Image</label>
                            <figure class="user-avatar medium">
                                <img class='ppanImg img-circle Image1' id="Image1" alt="avatar" src='img/avatar.png' style='height:65px;width:65px'>
                                <button id="pro_panimg" class="work-up-btn" onclick="document.getElementById('pro-panim').click(); return false;">Upload</button>
                                <input type='file' id="pro-panim" onchange="readURL(this);" style="visibility: hidden;">

                                <span id='ppan_status'></span>
                            </figure>
                            <span class="error" id="ppanUrlHTML"></span>
                        </div>
                        <div class="form-group">
                            <label for="ac_address">Personal Address</label>
                            <input type="text" class="form-control" id="pro_ac_address" placeholder="Personal Address" name="pro_ac_address">
                        </div>
                        <div class="row service-proof">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Address Proof Image</label>

                                    <figure class="user-avatar medium">
                                        <img class='addrImg Image1' alt="avatar" id="Image2" src='img/avatar.png' style='height:65px;width:65px'>
                                        <button id="equipment-photo" class="work-up-btn" onclick="document.getElementById('work-man-photo').click(); return false;">Upload</button>
                                        <input type='file' id="pro-adim" onchange="readURL(this);" style="visibility: hidden;">

                                        <span id='addr_status'></span>
                                    </figure>
                                    <span class="error" id="addrUrlHTML"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Cancelled Cheque Image Proof</label>

                                    <figure class="user-avatar medium">
                                        <img class='chequeImg Image1' alt="avatar" id="Image3" src='img/avatar.png' style='height:65px;width:65px'>
                                        <button id="equipment-photo" class="work-up-btn" onclick="document.getElementById('work-man-photo').click(); return false;">Upload</button>
                                        <input type='file' id="pro-cheim" onchange="readURL(this);" style="visibility: hidden;">

                                        <span id='cheque_status'></span>
                                    </figure>
                                    <span class="error" id="ccUrlHTML"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <label for="exampleInputEmail1">Note</label>
                            <textarea class="form-control" placeholder="Note"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <input type="button" id="pro-bankbtn" class="btn btn-primary" name="submit" value="Save Changes">
                            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- ends here -->
<script>

</script>

<?php
include 'footer.php'
?>