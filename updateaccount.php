<?php

include 'header.php';

?>

 <!-- start section -->
        <section class="section white-backgorund">
            <div class="container">
                <div class="row">
                    
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12 text-left">
                                <h2 class="title" style="margin-top: 10px;">My Account</h2>
                            </div><!-- end col -->
                        </div><!-- end row -->
                        <hr class="spacer-5"><hr class="spacer-20 no-border">
                        <div class="row" >
                            <div class="col-sm-2 text-left" >
                                <ul style="list-style-type: none;padding-left: 0px;">
                                    <li><a href="profille.php">Profile</a></li>
                                    <li><a href="updateaccount.php"><strong>Upgrade Account</strong></a></li>
                                    <li><a href="updatedoxservice.php">Update Document Service</a></li>
                                    <li><a href="updatedox.php">Update Document Product</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-10 text-left" style="border-left: 1px solid rgba(0, 0, 0, 0.1);" >
                                <h4 style="margin-top: 0px;">Upgrade Account</h4>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <img src="img/blog/blog_03.jpg" alt="avatar" width="250px" height="170px">
                                        <p><strong>Seller</strong></p>
                                        <a class="btn btn-default round btn-md" style="background-color: #1aff1a;border: 0px;" href="updatedox.php">Upgrade</a>
                                    </div>
                                    <div class="col-sm-4">
                                        <img src="img/blog/blog_04.jpg" alt="avatar" width="250px" height="170px">
                                        <p><strong>Service Provider</strong></p>
                                        <a class="btn btn-default round btn-md" style="background-color: #1aff1a;border: 0px;" id="serpro-update" data-toggle="modal" data-target="#upgradeAsSP">Upgrade</a>
                                    </div>


                                    <div class="col-sm-4">
                                            <img src="img/blog/blog_02.jpg" alt="avatar" width="250px" height="170px">
                                            <p><strong>Marketer</strong></p>
                                            <a class="btn btn-default round btn-md" style="background-color: #1aff1a;border: 0px;" href="refer.php">Upgrade</a>
                                        </div>
                                    
                                </div>
                                    <div class="row" style="margin-top: 20px;">
                                        <div class="col-sm-4">
                                            <img src="img/blog/blog_01.jpg" alt="avatar" width="250px" height="170px">
                                            <p><strong>Buyer</strong></p>
                                            <a class="btn btn-default round btn-md" href="#">Upgraded</a>
                                        </div>
                                        <div class="col-sm-4">
                                            <img src="img/blog/blog_03.jpg" alt="avatar" width="250px" height="170px">
                                            <p><strong>Seeker</strong></p>
                                            <a class="btn btn-default round btn-md" href="#">Upgraded</a>
                                        </div>
                                        <div class="col-sm-4">
                                            <img src="img/blog/blog_04.jpg" alt="avatar" width="250px" height="170px">
                                            <p><strong>Service User</strong></p>
                                            <a class="btn btn-default round btn-md" href="#">Upgraded</a>
                                        </div>
                                    </div>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>         


                                        <!-- Modal -->
                                        <div class="modal fade" id="upgradeAsSP" role="dialog">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                            <h5>Upgrade as  Service Provider</h5>
                                                        </div><!-- end modal-header -->
                                                        <div class="modal-body">
                                                        <!-- start of pop -->
                                                        <div class="form-group">
                                                        <label for="p_s_type">Post Service Details</label>
                                                            <ul>
                                                            <li style="list-style:none;">
                                                                <input type="radio" name="p_s_type" value="1" class="form-check-input" checked="" id="p_s_typeCompany">
                                                                Company
                                                                <input type="radio" name="p_s_type" class="form-check-input" value="2" id="p_s_typeIndiv">
                                                                Indivdual
                                                            </li>
                                                            </ul>
                                                        </div>
                                                        <!-- start of company -->
                                                        <div id="formCompany">
                                                            <form id="companyForm" action="" method="get">
                                                                <div class="form-group">
                                                                    <label for="c_type">Category Type</label>
                                                                    <select class="form-control" id="c_type" name="c_type">
                                                                    <!-- @foreach($datas as $dat) -->
                                                                    <!-- <option value='{{$dat["id"]}}'>{{$dat['name']}}</option> -->
                                                                    <option>Choose any one</option>
                                                                    <!-- @endforeach -->
                                                                    </select>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="c_name">Company Name :</label>
                                                                    <input type="text" class="form-control" id="c_name" placeholder="Company Name" name="c_name">
                                                                </div>

                                                                <div class="form-group"> 
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label for="c_gstin">GSTIN :</label>
                                                                            <input type="text" class="form-control" id="c_gstin" placeholder="GSTIN" name="c_gstin">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                                <label for="c_address">Company's Address :</label>
                                                                                <input type="text" class="form-control" id="c_address" placeholder="Address" name="c_address">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                

                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label for="multis">Region of service(States) :</label>
                                                                                <select id="multis">
                                                                                    <optgroup label="" value=""></optgroup>
                                                                                      
                                                                                          <option value=""></option>
                                                                                      
                                                                                </select>
                                                                        </div> <!-- col end -->
                                                                        <!-- <select id="multis">
                                                                                      
                                                                                          <option value="">choose any option</option>
                                                                                      
                                                                        </select> -->
                                                                        <div class="col-md-6">
                                                                            <label for="c_union">Region of service(Union Territories):</label>
                                                                            <select multiple="multiple" id="c_union">

                                                                                <optgroup label="Puducherry (Pondicherry)">
                                                                                    <option value="Karikal">Karikal</option>
                                                                                    <option value="Mahe">Mahe</option>
                                                                                    <option value="Yanam (Yanaon)">Yanam (Yanaon)</option>
                                                                                </optgroup>
                                                                              
                                                                            </select>

                                                                            
                                                                        </div>
                                                                    </div> <!-- row end -->
                                                                </div> <!-- formgroup end -->


                                                                <!-- <div class="form-group">
                                                                    <label for="c_serviceregion">Region of Service :</label>
                                                                    <select class="form-control" id="c_serviceregion" name="c_serviceregion">
                                                                    <option value="1">Chennai</option>
                                                                    </select>
                                                                </div>
 -->
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                    <div class="col-md-6">
                                                                    <label for="c_baddress">Billing Address :</label>
                                                                    <input type="text" class="form-control" id="c_baddress" placeholder="Billing Address" name="c_baddress">
                                                                    </div>
                                                                    <div class="col-md-6 checkalign">
                                                                    <input type="checkbox" name="sameforcompany" id="cloneaddress">  Click here if same as company's address.
                                                                    </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="c_cert">Certification if Available :</label>
                                                                    <input type="text" class="form-control" id="c_cert" placeholder="Certification" name="c_cert">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="c_tan">TAN Number :</label>
                                                                    <input type="text" class="form-control" id="c_tan" placeholder="TAN Number" name="c_tan">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="c_aadhar">Aadhar Number :</label>
                                                                    <input type="text" class="form-control" id="c_aadhar" placeholder="Aadhar Number" name="c_aadhar">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="c_pan">PAN Number :</label>
                                                                    <input type="text" class="form-control" id="c_pan" placeholder="PAN Number" name="c_pan">
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="checkbox-input checkbox-primary">
                                                                        <input id="checkbox2" class="styled" type="checkbox" checked="">
                                                                        <label for="checkbox2">
                                                                        I there by agree to the TERMS &amp; CONDITIONS of 7ATARA
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <center>
                                                                    <input type="submit" class="btn btn-default btn-lg" name="" id="postServiceDetailCompany" value="Post Service Detail">
                                                                </center>
                                                            </form>
                                                        </div>
                                                        <!-- end of company -->

                                                        <!-- start of individual -->
                                                        <div id="formIndividual" class="hidden">
                                                            <form id="individualForm" action="" method="get">
                                                                <div class="form-group">
                                                                    <label for="i_type">Category Type</label>
                                                                    <select class="form-control" id="i_type" name="i_type">
                                                                    <!-- @foreach($datas as $dat)
                                                                    <option value='{{$dat["id"]}}'>{{$dat['name']}}</option>
                                                                    @endforeach -->
                                                                    </select>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="i_name">Name :</label>
                                                                    <input type="text" class="form-control" id="i_name" placeholder="Name" name="i_name">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="i_address">Address :</label>
                                                                    <input type="text" class="form-control" id="i_address" placeholder="Address" name="i_address">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="i_cert">Certification if Available :</label>
                                                                    <input type="text" class="form-control" id="i_cert" placeholder="Certification" name="i_cert">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="i_service_city">Region of Service :</label>
                                                                    <select class="form-control" id="i_service_city" name="i_service_city">
                                                                    <option value="1">Chennai</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Proof of Identity</label>
                                                                    
                                                                    <figure class="user-avatar medium">
                                                                        <img class='poiImg' alt="avatar" src="img/avatar.png" style='height:65px;width:65px' >
                                                                        <span id='poi_pic' href="javascript:;" class="btn-md round">Upload Image</span>
                                                                        <span id='poi_status'></span>
                                                                    </figure>
                                                                    <input type='hidden' name='poiUrl'>
                                                                    <span class="error" id="poiUrlHTML"></span>
                                                                </div>
                                                                <div class="form-group" id="uploadForms">
                                                                    <label for="email">Upload Photograph</label>
                                                                    <figure class="user-avatar medium"> 
                                                                    
                                                                    <img class='userImg' alt="avatar" src="img/avatar.png" style='height:65px;width:65px' >
                                                                    <span id='cat_pic' href="javascript:;" class="button mid-short dark-light" >Upload Image</span>
                                                                    </figure>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="email">Machinery equipments of vehicle involved ? :</label>
                                                                    <div class="radio-input radio-primary">
                                                                        <input type="radio" name="i_machine" id="i_machine" value="3">
                                                                        <label for="radio3">
                                                                        Yes
                                                                        </label>
                                                                        <input type="radio" name="i_machine" id="i_machine2" value="4" checked="">
                                                                        <label for="i_machine2">
                                                                        No
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="checkbox-input checkbox-primary">
                                                                        <input id="checkbox2" class="styled" type="checkbox" checked="">
                                                                        <label for="checkbox2">
                                                                        I there by agree to the TERMS &amp; CONDITIONS of 7ATARA
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <center>
                                                                    <input type="submit" class="btn btn-default btn-lg" value="Post Service Details">
                                                                </center>   
                                                            </form>
                                                        </div>
                                                        <!-- end of individual -->
                                                            <!-- end of pop -->
                                                        </div><!-- end modal-body -->
                                                    </div><!-- end modal-content -->
                                                </div><!-- end modal-dialog -->
                                            </div>
                                        <!-- end here-->


<script>
  $(function() {
    $('#c_state').multipleSelect({
      multiple: true,
      width: 440,
      multipleWidth: 55
    })
    $('#c_union').multipleSelect({
      multiple: true,
      width: 420,
      multipleWidth: 55
    })
  })
</script>




<?php

include 'footer.php';

?>